# Tasks schema
 
# --- !Ups

CREATE SEQUENCE orga_id_seq;
CREATE TABLE organisation (
    id integer NOT NULL DEFAULT nextval('orga_id_seq'),
    name varchar(255) NOT NULL
);

CREATE SEQUENCE user_id_seq;
CREATE TABLE user (
    id integer NOT NULL DEFAULT nextval('user_id_seq'),
    name varchar(255) NOT NULL,
    lastname varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255),
    organisation_id integer NOT NULL REFERENCES organisation(id)
);


 
# --- !Downs
 

DROP TABLE user;
DROP SEQUENCE user_id_seq;

DROP TABLE organisation;
DROP SEQUENCE orga_id_seq;