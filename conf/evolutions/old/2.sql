# --- Sample dataset

# --- !Ups

insert into organisation (id, name) values (1, 'first');
insert into organisation (id, name) values (2, 'second');
insert into organisation (id, name) values (3, 'third');


insert into user (name,lastname,email,password,organisation_id) values ('john', 'doe', 'john.doe@example.com', 'secret', 1);
insert into user (name,lastname,email,password,organisation_id) values ('bill', 'smith', 'bill.smith@example.com', 'secret', 2);


# --- !Downs

delete from user;
delete from organisation;

