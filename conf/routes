# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

#GET     /                               controllers.auth.Authentication.views_enter

# Home page
GET     /                               controllers.Application.root(any = "none")
GET     /views/index                    controllers.Application.index
GET     /views/contact                  controllers.Application.contact
GET     /views/home                     controllers.Application.home
GET     /views/about                    controllers.Application.about

######### AUTHENTICATION ################

GET     /views/register                 controllers.auth.Authentication.views_register
GET     /views/login                    controllers.auth.Authentication.views_login

POST    /api/register                   controllers.auth.Authentication.register

POST    /api/login                      controllers.auth.Authentication.login
POST    /api/login_form                 controllers.auth.Authentication.login_form
POST    /api/logout                     controllers.auth.Authentication.logout
GET     /api/ping                       controllers.auth.Authentication.ping

######### USER RELATED #####################

GET     /views/messages                 controllers.Messages.index
GET     /api/messages                   controllers.Messages.myMessages(skip: Int ?=0, limit: Int ?=10)


########## USERS ############### 

GET       /views/users                  controllers.Users.index
GET       /views/user                   controllers.Users.display

GET       /api/users                    controllers.Users.list(skip: Int ?=0, limit: Int ?=10)
GET       /api/users                    controllers.Users.count

#POST      /api/users                    controllers.Users.create
GET       /api/users/:id                controllers.Users.retrieve(id: String)
PUT       /api/users/:id                controllers.Users.update(id: String)
DELETE    /api/users/:id                controllers.Users.delete(id: String)

#GET       /api/users/:id/history      controllers.Users.getHistory(id: String, skip: Int ?= 0, limit: Int ?=100)

GET       /api/profile                  controllers.Users.profile

########## ORGANISATIONS #######

GET     /views/organisations            controllers.Organisations.index
GET     /views/organisation             controllers.Organisations.display

GET     /api/organisations              controllers.Organisations.list(skip: Int ?=0, limit: Int ?=10)
GET     /api/organisations/count        controllers.Organisations.count

POST    /api/organisations              controllers.Organisations.create
GET     /api/organisations/:id          controllers.Organisations.retrieve(id: String)
PUT     /api/organisations/:id          controllers.Organisations.update(id: String)
DELETE  /api/organisations/:id          controllers.Organisations.delete(id: String)

######### TICKETS #############

GET     /views/tickets                  controllers.Tickets.index
GET     /views/ticket                   controllers.Tickets.display

GET     /api/tickets                    controllers.Tickets.list(skip: Int ?= 0, limit: Int ?=10)
GET     /api/tickets/count              controllers.Tickets.count

POST    /api/tickets                    controllers.Tickets.create
GET     /api/tickets/:id                controllers.Tickets.retrieve(id: String)
PUT     /api/tickets/:id  	            controllers.Tickets.update(id: String)
DELETE  /api/tickets/:id			    controllers.Tickets.delete(id: String)

GET     /api/tickets/:id/comments       controllers.Tickets.getComments(id: String)
POST    /api/tickets/:id/comments       controllers.Tickets.addComment(id: String)

GET     /api/tickets/:id/history       controllers.Tickets.getHistory(id: String, skip: Int ?= 0, limit: Int ?=100)

########## ROLES #######

GET     /views/roles            controllers.Roles.index
GET     /views/role             controllers.Roles.display

GET     /api/roles              controllers.Roles.list(skip: Int ?=0, limit: Int ?=10)
GET     /api/roles/!count       controllers.Roles.count
GET     /api/roles/!permissions controllers.Roles.permissions

POST    /api/roles              controllers.Roles.create
GET     /api/roles/:id          controllers.Roles.retrieve(id: String)
PUT     /api/roles/:id          controllers.Roles.update(id: String)
DELETE  /api/roles/:id          controllers.Roles.delete(id: String)


######### EVENTS ###############

GET     /views/events               controllers.MonitoringEvents.index

POST    /api/events                 controllers.MonitoringEvents.create
GET     /api/events                 controllers.MonitoringEvents.list(skip: Int ?=0, limit: Int ?=50)

######### COMMENTS ###############

GET     /views/comments               controllers.Comments.index

POST    /api/comments                 controllers.Comments.create
GET     /api/comments                 controllers.Comments.list(skip: Int ?=0, limit: Int ?=10)

######### DICTIONARIES ###############

# GET     /views/dictionaries               controllers.Dictionaries.index

GET     /api/dictionaries                 controllers.Dictionaries.list(skip: Int ?=0, limit: Int ?=10)
GET     /api/dictionaries/:id                controllers.Dictionaries.retrieve(id: String)

# Map static resources from the /public folder to the /assets URL path
GET     /assets/*file                   controllers.Assets.at(path="/public", file)


GET     /*any                           controllers.Application.root(any)
