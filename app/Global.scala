
import actors.{Monitor_Mock, Monitor_HTTP, Agent, Monitoring, Notify, TicketActor}

import akka.actor.{Cancellable, ActorSystem}
import events.{TicketEvent, EventBus}

import models.{MonitoringEvent, Message}
import play.api.libs.concurrent.Akka

// import libs.concurrent.Akka
import play.api.{Application, Logger, GlobalSettings}
import scala.concurrent.duration._

import scala.collection.mutable

object Global extends GlobalSettings {

//  implicit val app = play.api.Play.current
  implicit val context = scala.concurrent.ExecutionContext.global

  // ActorSystem is a heavy object: create only one per application
  val system = ActorSystem("mySystem")
  val ticketActor = system.actorOf(TicketActor.props)
  val notifying = system.actorOf(Notify.props)
  val monitoring = system.actorOf(Monitoring.props)
  val agent = system.actorOf(Agent.props)

  val monitors = new mutable.ListBuffer[Cancellable]

  override def onStart(app: Application) {
    implicit val current = app
    Logger.info("Application has started")

    EventBus.subscribe(ticketActor, classOf[TicketEvent])
    EventBus.subscribe(notifying, classOf[Message])
    EventBus.subscribe(monitoring, classOf[MonitoringEvent])

    Logger.info("Scheduling monitoring")

    // CPU monitoring
    monitors += Akka.system.scheduler.schedule(
      3.seconds, 1.minute, agent, "cpu"
    )

    monitors += Akka.system.scheduler.schedule(
      3.seconds, 1.minute, agent, Monitor_HTTP("http://www.google.fr")
    )

    monitors += Akka.system.scheduler.schedule(
      3.seconds, 2.minutes, agent, Monitor_HTTP("http://fr.yahoo.com")
    )
    
    monitors += Akka.system.scheduler.schedule(
      3.seconds, 3.minute, agent, Monitor_HTTP("http://www.lemonde.fr")
    )

    /*
    monitors += Akka.system.scheduler.schedule(
      3.seconds, 10.seconds, agent, Monitor_Mock(0, 0, 10)
    )
    * 
    */

    Logger.info("scheduled")
  }

  override def onStop(app: Application) {
    Logger.info("Application shutdown...")
    monitors.foreach {
       c => c.cancel()
    }
  }

}
