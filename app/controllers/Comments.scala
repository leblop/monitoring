package controllers

import models.{Role, Comment}
import api.{DAOController, SecuredController}

/**
 * Created with IntelliJ IDEA.
 * User: matthieu
 * Date: 01/10/13
 * Time: 19:55
 * To change this template use File | Settings | File Templates.
 */

object Comments extends SecuredController  {

  /** ******************** API ********************/

  // implicit val context = scala.concurrent.ExecutionContext.global

  def dao = DAOController.get(Comment)

  def create = Secured.async(parse.json) { implicit request => dao.create }

  // def retrieve(name: String) = Secured.async { implicit request => dao.retrieve(name) }

  // def update(name: String) = Secured.async(parse.json) { implicit request => dao.update(name) }

  // def delete(name: String) = Secured.async(parse.json) { implicit request => dao.delete(name) }

  def list(skip: Int = 0, limit: Int = 10) = Secured.async { implicit request => dao.list(skip, limit)  }

  def count = Secured.async { implicit request => dao.count }

  /** ******** VIEWS ************* **/

  def index = Secured { Ok(views.html.comments()) }
}
