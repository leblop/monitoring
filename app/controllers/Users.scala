package controllers

import models.User
import api.{DAOController, SecuredController}


object Users extends SecuredController {

  // implicit val context = scala.concurrent.ExecutionContext.global

  val dao = DAOController.get(User)

  // def create = Secured.async(parse.json) { implicit request => dao.createWith(factory)  }

  def retrieve(name: String) = Secured.async { implicit request => dao.retrieve(name) }

  def update(name: String) = Secured.async(parse.json) { implicit request => dao.update(name) }

  def delete(name: String) = Secured.async(parse.json) { implicit request => dao.delete(name) }

  def list(skip: Int = 0, limit: Int = 10) = Secured.async { implicit request => dao.list(skip, limit)  }

  def count = Secured.async { implicit request => dao.count }

  def profile = Secured.async{ implicit request => dao.retrieve(request.currentUser.username)}


  /** ******** VIEWS ************* **/

/*
  override def update(id: String) =  authenticated {
    process {
      processor =>
        (processor
          |+ User.update(id)
          // |+ TicketEvent.fire(TicketUpdate(id, processor.user))
          |- log(Logger.error)
          |+ log(Logger.info)
          |+ clearContent
          |+ msg_info("User updated")
          )
    }
  }
*/
  def index = Secured {
    Ok(views.html.users())
  }

  def display = Secured {
    Ok(views.html.user())
  }
}