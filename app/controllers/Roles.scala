package controllers


import api.{SecuredRequest, DAOController, SecuredController}
import models.{Permission, User, Ticket, Role}
import play.api.libs.json._
import org.joda.time.DateTime
import reactivemongo.bson.BSONObjectID
import api.SecuredRequest
import helpers.JSONHelpers._
import helpers.converters.JsResultAsFuture
import api.SecuredRequest

object Roles extends SecuredController {

  // implicit val context = scala.concurrent.ExecutionContext.global


  def dao = DAOController.get(Role)

  def create = Secured.async(parse.json) { implicit request =>
    for {
      role <- roleFrom(request).asFuture
      _ <- Role.insert(role)
    } yield Created(Json.obj(
      success("%s created".format(role.name)),
      url(role.url)
    ))
  }

  def retrieve(name: String) = Secured.async { implicit request => dao.retrieve(name) }

  def update(name: String) = Secured.async(parse.json) { implicit request => dao.update(name) }

  def delete(name: String) = Secured.async(parse.json) { implicit request => dao.delete(name) }

  def list(skip: Int = 0, limit: Int = 10) = Secured.async { implicit request => dao.list(skip, limit)  }

  def count = Secured.async { implicit request => dao.count }

  def permissions = Secured { implicit request =>
    Ok(Json.obj(
      "permissions" -> Permission.assignable
    ))
  }

  def index = Secured { implicit request =>
    Ok(views.html.roles())
  }

  def display = Secured { implicit request =>
    Ok(views.html.role())
  }

  import play.api.libs.functional.syntax._

  // Factory method to create an organisation from a jsValue
  private def roleFrom(request: SecuredRequest[JsValue]) = roleReads.reads(request.body)

  private val roleReads : Reads[Role] = (
    (__ \ "key").read[String] and
      (__ \ "name").read[String]).apply(createRole _)

  private def createRole(key: String, name: String) = {
    val now = DateTime.now
    Role(
      _id = BSONObjectID.generate,
      creation_date = now,
      update_date = now,
      key = key,
      name = name,
      permissions = Set.empty)
  }


}

