package controllers

import api.{DAOController, SecuredController}
import models.{Dictionary, Role}

object Dictionaries extends SecuredController {

  // implicit val context = scala.concurrent.ExecutionContext.global

  def dao = DAOController.get(Dictionary)

  def create = Secured.async(parse.json) { implicit request => dao.create }

  def retrieve(name: String) = Secured.async { implicit request => dao.retrieve(name) }

  def update(name: String) = Secured.async(parse.json) { implicit request => dao.update(name) }

  def delete(name: String) = Secured.async(parse.json) { implicit request => dao.delete(name) }

  def list(skip: Int = 0, limit: Int = 10) = Secured.async { implicit request => dao.list(skip, limit)  }

  def count = Secured.async { implicit request => dao.count }

  // def index = Secured { Ok(views.html.dictionaires()) }

}

