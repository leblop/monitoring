package controllers

import models.Organisation
import api.{DAOController, SecuredRequest, HttpException, SecuredController}
import play.api.libs.json.JsValue
import org.joda.time.DateTime
import reactivemongo.bson.BSONObjectID


object Organisations extends SecuredController  {

  // implicit val context = scala.concurrent.ExecutionContext.global

  def dao = DAOController.get(Organisation)

  def create = Secured.async(parse.json) { implicit request => dao.createWith(factory) }

  def retrieve(name: String) = Secured.async { implicit request => dao.retrieve(name) }

  def update(name: String) = Secured.async(parse.json) { implicit request => dao.update(name) }

  def delete(name: String) = Secured.async(parse.json) { implicit request => dao.delete(name) }

  def list(skip: Int = 0, limit: Int = 10) = Secured.async { implicit request => dao.list(skip, limit)  }

  def count = Secured.async { implicit request => dao.count }


  def index = Secured { implicit request =>
    Ok(views.html.organisations())
  }

  def display = Secured { implicit request =>
    Ok(views.html.organisation())
  }

  // Factory method to create an organisation from a jsValue
  private val factory = (request: SecuredRequest[JsValue]) => {
    val now = DateTime.now
    val value = request.body

    for {
      name <- (value \ "name").validate[String]
      description <- (value \ "description").validate[String]
    } yield Organisation(
      BSONObjectID.generate,
      now,
      now,
      name.toLowerCase.replaceAll(" ", "_"),
      name,
      description
    )
  }
}

/*
object Organisations extends DAOController with CRUD_API {

  val dao = Organisation


  /** ********* API ************** */


  /** ******** VIEWS ************* **/

  def index = restricted_view {
      Ok(views.html.organisations())
  }

  def display = restricted_view {
      Ok(views.html.organisation())
  }

}
  */