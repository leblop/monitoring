package controllers

import play.api.mvc._
import api.SecuredController


object Application extends SecuredController {

  def root(any: String) = Action { implicit request => Ok(views.html.root()) }

  def index = Secured { Ok(views.html.index()) }
  def home = Secured { Ok(views.html.home()) }
  def about = Secured { Ok(views.html.about()) }
  def contact = Secured { Ok(views.html.contact()) }

}