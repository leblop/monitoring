package controllers

import models.{Pagination, Message}
import api.SecuredController
import concurrent.Future
import play.api.libs.json.Json
import helpers.JSONHelpers.pagination

/**
 * Created with IntelliJ IDEA.
 * User: matthieu
 * Date: 01/10/13
 * Time: 19:55
 * To change this template use File | Settings | File Templates.
 */

object Messages extends SecuredController {

  /** ******************** API ********************/

  val dao = Message

  // implicit val context = scala.concurrent.ExecutionContext.global

  /*
  def count =  Action { Forbidden }
  def list(skip: Int,limit: Int) =  Action { Forbidden }
  def create =  Action { Forbidden }
  def retrieve(id: String) =  Action { Forbidden }
  def update(id: String) =  Action { Forbidden }
  def delete(id: String) =  Action { Forbidden }
  */

  def myMessages(skip: Int, limit: Int) = Secured.async{ implicit request =>
    for {
      messages <- Message.myMessages(request.currentUser._id)
      count = messages.length
    } yield Ok(Json.obj(
      Message.collectionName -> messages,
      pagination(count, 0, count)
    ))
  }

  /** ******** VIEWS ************* **/

  def index =  Secured { Ok(views.html.messages()) }
}
