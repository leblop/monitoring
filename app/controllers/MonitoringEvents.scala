package controllers

import play.api.mvc.{Results, Action, Request}

import models.{MonitoringEvent}

import api.{DAOController, HttpException, SecuredController}
import play.api.libs.json.{JsValue, Json}


/**
 * Created with IntelliJ IDEA.
 * User: matthieu
 * Date: 01/10/13
 * Time: 19:55
 * To change this template use File | Settings | File Templates.
 */


object MonitoringEvents extends SecuredController {

  import helpers.converters.TryAsFuture

  // implicit val context = scala.concurrent.ExecutionContext.global

  def dao = DAOController.get(MonitoringEvent)

  def create = Action.async(parse.json) { implicit request =>
    for {
      event <- MonitoringEvent.fromJson(request.body).asFuture
      _ <- MonitoringEvent.insert(event)
    } yield Created(event.end_date.getMillis.toString)
  }

  def list(skip: Int = 0, limit: Int = 10) = Secured.async { implicit request => dao.list(skip, limit)  }

  def count = Secured.async { implicit request => dao.count }

  // val dao = MonitoringEvent

  /** ******************** API ********************/

  /**
   * Add a new event to base
   * @return
   */
  /*
  override def create = Action.async(parse.json) { implicit request: Request[JsValue] =>
    import events.MonitoringEvent._
    (Processor(OK)(request, User.anonymous)
      |+ MonitoringEvent.create
      |+ http(ACCEPTED)
      |+ fire(MONITORING_EVENT)
    ).toResult
  }
  */

  /** ******** VIEWS ************* **/

  def index =  Secured { _ => Ok(views.html.monitoring_events()) }
}
