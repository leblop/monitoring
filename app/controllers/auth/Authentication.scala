package controllers.auth

import api.{HttpException, SecuredController}
import helpers.JSONHelpers.resKO
import concurrent.Future
import models.{Token, Password, User}
import play.api.cache._
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json._
import play.api.mvc._
import reactivemongo.bson.BSONObjectID
import org.joda.time.DateTime
import helpers.BCryptHelper
import helpers.converters.JsResultAsFuture


object Authentication extends SecuredController {


  lazy val CacheExpiration =
    app.configuration.getInt("cache.expiration").getOrElse(60 /*seconds*/ * 60 /* minutes */ * 24 /* hours */)

  import play.api.libs.functional.syntax._

  private val userReads : Reads[User] = (
      (__ \ "username").read[String] and
      (__ \ "firstname").read[String] and
      (__ \ "lastname").read[String] and
      (__ \ "email").read[String]
    ).apply(createUser _)

  private def createUser(
      username: String,
      firstname: String,
      lastname: String,
      email: String) = {
    val now = DateTime.now
    User(
      _id = BSONObjectID.generate,
      creation_date = now,
      update_date = now,
      username = username,
      firstname = firstname,
      lastname = lastname,
      email = email)
    }

  private def createPassword(user: User, value: JsValue) =
    for {
      password <- (value \ "password").validate[String]
    } yield Password(user._id, BCryptHelper.hash(password))

  def register = Public.async(parse.json) { implicit request =>
    for {
      user  <- userReads.reads(request.body).asFuture
      pwd   <- createPassword(user, request.body).asFuture
      _     <- User.insert(user)
      _     <- Password.insert(pwd)
    } yield Created(user.url)
  }

  val views_register = Action {Ok(views.html.auth.register())}

  private case class Login(username: String, password: String, redirectTo: Option[String])

  private val loginForm = Form(
    mapping(
      "username" -> nonEmptyText,
      "password" -> nonEmptyText,
      "redirectTo" -> optional(text)
    )(Login.apply)(Login.unapply)
  )

  implicit class ResultWithToken(result: SimpleResult) {
    def withToken(token: (String, BSONObjectID)): SimpleResult = {
      Cache.set("tokens-" + token._1, token._2.stringify, CacheExpiration)
      Token.insert(token._1, token._2)
      result.withCookies(Cookie(AuthTokenCookieKey, token._1, None, httpOnly = false))
    }

    def discardingToken(token: String): SimpleResult = {
      Cache.remove(token)
      result.discardingCookies(DiscardingCookie(name = AuthTokenCookieKey))
    }
  }


  /** Check credentials, generate token and serve it back as auth token in a Cookie */

  def login = Public.async(parse.json) { implicit request =>
    loginForm.bind(request.body).fold( // Bind JSON body to form values
      formErrors => Future.successful(BadRequest(resKO(formErrors.errorsAsJson))),
      loginData =>
        User.authenticate(loginData.username, loginData.password) map { user =>
            val token = java.util.UUID.randomUUID().toString
            Ok(Json.obj(
              "authToken" -> token,
              "userId" -> user.username
            )).withToken(token -> user._id)
        }
    )
  }

  def login_form = Public.async { implicit request =>
    loginForm.bindFromRequest.fold( // Bind JSON body to form values
      formErrors => Future.successful(BadRequest(resKO(formErrors.errorsAsJson))),
      loginData =>
          User.authenticate(loginData.username, loginData.password) map { user =>
              val token = java.util.UUID.randomUUID().toString
              loginData.redirectTo.map(url => Redirect(url)).getOrElse(Redirect(controllers.routes.Application.root("none")))
                .withToken(token -> user._id)
          }
    )
  }

  /** Invalidate the token in the Cache and discard the cookie */
  def logout = Action { implicit request =>
    request.headers.get(AuthTokenHeader) map { token =>
      Redirect("/").discardingToken(token)
    } getOrElse BadRequest(resKO("No Token"))
  }

  /**
   * Returns the current user's ID if a valid token is transmitted.
   * Also sets the cookie (useful in some edge cases).
   * This action can be used by the route service.
   */
  // def ping() = Security.Authenticated(username, onUnauthorized) {
  //  implicit user => Action { implicit request =>
  def ping = Secured { implicit request =>
      val user = request.currentUser
      hasToken(request).map(token => Ok(Json.obj("userId" -> user.username)).withToken(token -> user._id)).getOrElse(Unauthorized)
    }

   val views_login = Public {Ok(views.html.auth.login())}


}