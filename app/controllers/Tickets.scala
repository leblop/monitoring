package controllers


import models.{Identifier, User, Comment, TicketHistory, TicketDiff, History, Ticket}
import api.{HttpException, DAOController, SecuredRequest, SecuredController}
import play.api.libs.json._
import org.joda.time.DateTime
import reactivemongo.bson.BSONObjectID
import play.api.mvc.Results
import scala.util.{Failure, Success, Try}
import helpers.converters.{TryAsFuture, JsResultAsFuture}
import helpers.JSONHelpers.{success, url}
import events.{TicketUpdate, TicketCreate, EventBus}
import helpers.JSONHelpers.validate
import concurrent.Future
import scala.util.Failure
import events.TicketUpdate
import events.TicketCreate
import scala.util.Success
import api.SecuredRequest

object Tickets extends SecuredController {


  // implicit val context = scala.concurrent.ExecutionContext.global

  val dao = DAOController.get(Ticket)

  def create = Secured.async(parse.json) { implicit request =>
    for {
      key     <- Ticket.next
      ticket  <- ticketFrom(key, request).asFuture
      _       <- Ticket.insert(ticket)
    } yield
    {
      TicketHistory.add(ticket, "created")
      EventBus.publish(TicketCreate(TicketDiff(ticket), request.currentUser))
      Created(Json.obj(
        success("%s created".format(ticket.name)),
        url(ticket.url)
      ))
    }
  }

  def retrieve(name: String) = Secured.async { implicit request => dao.retrieve(name) }

  def update(name: String) = Secured.async(parse.json) { implicit request =>
    for {
      ticket    <- Ticket.fromJson(request.body).asFuture
      oldTicket <- Ticket.getByKey(name)
      newTicket <- validateUpdate(ticket, oldTicket).asFuture
      _         <- Ticket.update(name, newTicket)
    } yield {
      val diff = TicketDiff(newTicket, oldTicket)
      TicketHistory.add(newTicket, "update")
      EventBus.publish(TicketUpdate(diff, request.currentUser))
      Results.Ok(Json.obj("success" -> "%s updated".format(name)))
    }
  }

  def getHistory(name: String, skip: Int, limit: Int) = Secured.async{ implicit request =>
    for {
      ticket    <- Ticket.getByKey(name)
      entries <- TicketHistory.find(ticket.uid)
    } yield Ok(Json.obj("history" -> entries))
  }

  def getComments(name: String) = Secured.async { implicit request =>
    for {
      ticket    <- Ticket.getByKey(name)
      comments  <- Comment.find(ticket.uid)
    } yield Ok(Json.obj("comments" -> comments))
  }

  def addComment(name: String) = Secured.async(parse.json) { implicit request =>
    for {
      content <- Comment.contentReads.reads(request.body).asFuture
      ticket  <- Ticket.getByKey(name)
      _       <- Comment.add(ticket, content)
    } yield Created("")
  }


  private def validateUpdate(newTicket: Ticket, oldTicket: Ticket) : Try[Ticket] = {
    for {
      _ <- check (newTicket._id == oldTicket._id, "ID cannot be changed")
      _ <- check (newTicket.creation_date == oldTicket.creation_date, "creation date cannot be changed")
      _ <- check (newTicket.update_date == oldTicket.update_date,     "update date cannot be changed")
      _ <- check (newTicket.reporter._id == oldTicket.reporter._id,   "reporter cannot be changed")
    } yield newTicket.touch
  }

  private def check(condition: => Boolean, msg: => String) : Try[Unit] = if( ! condition ) Failure(HttpException(FORBIDDEN, msg)) else Success()

  private def isSet(value: Option[String], msg: => String) = check(value.map(! _.isEmpty).getOrElse(false), msg)




  def delete(name: String) = Secured.async(parse.json) { implicit request => dao.delete(name) }

  def list(skip: Int = 0, limit: Int = 10) = Secured.async { implicit request => dao.list(skip, limit)  }

  def count = Secured.async { implicit request => dao.count }

  // Factory method to create an organisation from a jsValue
  private val ticketFrom = (key: String, request: SecuredRequest[JsValue]) => {
    val now = DateTime.now
    val value = request.body
    val _id = BSONObjectID.generate
    for {
      title <- (value \ "title").validate[String]
      description <- (value \ "description").validate[String]
    } yield Ticket(
      _id,
      now,
      now,
      key,
      title,
      description,
      "",
      request.currentUser.toEmbedded,
      request.currentUser.toEmbedded,
      None
    )
  }




  /** ********* API ************** */
/*
  override def create =  authenticated { process {
    processor =>
      implicit val user = processor.user
      (processor
        |+ transform(Ticket.addReporter andThen Ticket.addRequester)
        |+ Ticket.create
        |+ withId((id, p) => TicketEvent.fire(TicketCreate(id, p.user))(p))
        |- log(Logger.error)
        |+ log(Logger.info)
        |+ clearContent
      )
    }
  }

  override def update(id: String) =  authenticated {
    process {
      processor =>
        (processor
        |+ Ticket.update(id)
        |+ TicketEvent.fire(TicketUpdate(id, processor.user))
        |- log(Logger.error)
        |+ log(Logger.info)
        |+ clearContent
        )
    }
  }

  override def delete(id: String) =  authenticated {
    process {
      processor =>
        (processor
        |+ Ticket.delete(id)
        |+ fire(TicketDelete(id, processor.user))
        |- log(Logger.error)
        |+ log(Logger.info)
        |+ clearContent
        )
    }
  }
 */
  /** ******** VIEWS ************* **/

  def index = Secured {
      Ok(views.html.tickets())
  }

  def display = Secured {
      Ok(views.html.ticket())
  }
}