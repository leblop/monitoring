package processors


import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json.{JsObject, Reads}
import play.api.mvc.{Request, SimpleResult}
import play.api.mvc.Results._
import play.api.libs.json.{JsObject, Reads, JsValue, Json}
import concurrent.Future
import models.User
import play.api.http.Status._


import play.api.libs.concurrent.Execution.Implicits._
import scala.Predef._
import play.api.mvc.AsyncResult

/**
 * Created with IntelliJ IDEA.
 * User: matthieu
 * Date: 05/12/13
 * Time: 21:37
 * To change this template use File | Settings | File Templates.
 */

/*
sealed trait Processor {

  /**
   * Execute the provided method, returning a processor for next steps
   * @param f
   * @return
   */
  def execute (f: PlainProcessor => Processor) : Processor

  /**
   * Execute the provided method only if current result is successful (http code 2xx)
   * @param f
   * @return
   */
  def executeOnOk (f: PlainProcessor => Processor) : Processor

  /**
   * Execute the provided method only if current result is not successful (http code != 2xx)
   * @param f
   * @return
   */
  def executeOnError (f: PlainProcessor => Processor) : Processor

  /**
   * Shortcut for @see execute
   * @param f
   * @return
   */
  def | (f: PlainProcessor => Processor) : Processor = execute(f)

  /**
   * Shortcut for @see executeOnOk
   * @param f
   * @return
   */
  def |+ (f: PlainProcessor => Processor) : Processor = executeOnOk(f)

  /**
   * Shortcut for @see executeOnError
   * @param f
   * @return
   */
  def |- (f: PlainProcessor => Processor) : Processor = executeOnError(f)

  /**
   * Set the current http code
   *
   * @param code
   * @return
   */
  def http(code: Int) : Processor

  /**
   * Set the content to be returned to requester
   *
   * @param value
   * @return
   */
  def content(value: JsValue) : Processor

  /**
   * Return the result to requester
   * @return
   */
  def toResult : Future[SimpleResult]

  /**
   *
   */
  def content: Future[JsValue]

  /**
   * the requester
   * @return
   */
  def user : User
 }


object Processor {

  def apply(code: Int)(implicit request: Request[JsValue],  user: User): PlainProcessor = PlainProcessor(code, request.body, user)

  def apply(code: Int, value: JsValue)(implicit  user: User): PlainProcessor = PlainProcessor(code, value, user)
}



case class PlainProcessor(
     code: Int,
     value: JsValue,
     implicit val user: User)  extends Processor {

  private def success(value: Int): Boolean = 200 until 299 contains value

  def execute (f: PlainProcessor => Processor) : Processor = f(this)

  def executeOnOk(f: PlainProcessor => Processor) : Processor =
    code match {
      case v if success(v) => execute(f)
      case _ => this
    }

  def executeOnError (f: PlainProcessor => Processor) : Processor =
    code match {
      case v if ! success(v) => execute(f)
      case _ => this
    }

  def http(code: Int) : Processor  = this.copy(code = code)

  def content(value: JsValue) : Processor = this.copy(value = value)

  def content: Future[JsValue] = Future.successful(value)

  def toResult : Future[SimpleResult]  = Future.successful(new Status(code)(value))

  def toFuture(result: Future[Processor]) : Processor =  FutureProcessor(result, user)

  def fork(value: JsValue)(f: PlainProcessor => Processor) : Processor =
  {
    this.copy(value = value).execute(f)
    this
  }
}

case class FutureProcessor(
                            result: Future[Processor],
                            implicit val user: User) extends Processor {

  def toResult : Future[SimpleResult] = result.flatMap(_.toResult)

  def execute (f: PlainProcessor => Processor) : Processor        = this.copy(result = result.map(_.execute(f)))
  def executeOnOk (f: PlainProcessor => Processor) : Processor    = this.copy(result = result.map(_.executeOnOk(f)))
  def executeOnError (f: PlainProcessor => Processor) : Processor = this.copy(result = result.map(_.executeOnError(f)))

  def http(code: Int) : Processor  = this.copy(result = result.map(_.http(code)))

  def content(value: JsValue) : Processor = this.copy(result = result.map(_.content(value)))

  def content : Future[JsValue] = result.flatMap(_.content)

}
    */





