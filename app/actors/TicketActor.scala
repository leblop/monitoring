package actors

import akka.actor.{Props, Actor}
import play.api.Logger
import models.Ticket
import events.TicketEvent._
import events.{TicketEvent, TicketCreate}
import models.Message

import concurrent.ExecutionContext.Implicits.global

/**
 * Created by matthieu on 17/12/13.
 */
class TicketActor extends Actor
{

  def receive = {
    case event: TicketCreate => onTicketCreate(event)
    case event: TicketEvent if event.name == TICKET_UPDATE => onTicketUpdate(event)
  }

  private def onTicketCreate(event: TicketCreate) =
  {
    implicit val sender = event.user.toEmbedded
    val ticket = event.ticket
    val title = "Ticket created: [" + ticket.title.current + "] by " + ticket.reporter.current.name
    val recipient = ticket.reporter.current
    val content = views.html.mails.ticket_create(ticket)

    val msg = Message.send(recipient, title, content.toString)

  }

  private def onTicketUpdate(event: TicketEvent) =
  {
    implicit val sender = event.user.toEmbedded
    val ticket = event.ticket
    val title = "Ticket updated: [" + ticket.title.current + "]"
    val recipient = ticket.reporter.current
    val content = views.html.mails.ticket_update(ticket)

    val msg = Message.send(recipient, title, content.toString)


    /*
    val ticket = Ticket.fromJson(event.processor.value)
    val title = "Ticket updated: '" + ticket.map(_.title).getOrElse("undefined") + "'"
    val content = ticket.map(_.description).getOrElse("undefined")
    val recipient = ticket.map(_.reporter).getOrElse(User.anonymous.toEmbedded)

    (event.processor
      |+ Message.send(recipient, title, content)
      |+ log(Logger.info, "Ticket updated: " + title)
      |- log(Logger.error, "Error updating ticket: " + title)
      )
    */
  }

}

object TicketActor
{
  def props: Props = Props(classOf[TicketActor])
}
