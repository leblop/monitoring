package actors

import akka.actor.{Props, Actor}
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{Writes, JsNumber, Json}
import play.api.libs.ws.WS
import reactivemongo.bson.BSONObjectID
import models.{Priority, MonitoringEvent}

/**
 * Created by matthieu on 17/12/13.
 */
class Agent extends Actor
{


  object server {
    private val host = WS.url("http://localhost:9000/api/monitoring")

    def post(event: MonitoringEvent)(implicit writer: Writes[MonitoringEvent]) = host.post(writer.writes(event)).map
      {
          response => Logger.info("sent %s:\t%s\t[%s]\t%s".format(event.metric, event.value.toString, response.status.toString, event.target))
      }
  }

  // should be taken from configuration ?
  private val source = "georges"

  private def now = JsNumber(DateTime.now.getMillis )

  def receive = {
    case "cpu" => monitor_cpu
    case Monitor_HTTP(url) => monitor_http(url)
    case mock: Monitor_Mock => monitor_mock(mock)
  }

  private def monitor_http(url: String) = {
    def event = MonitoringEvent(
      source = "georges",
      metric="http",
      value = 0,
      target = url,
      priority = Priority.DANGER
    )
    WS.url(url).get.map {
      response => Monitoring process event.copy(
        value = response.status,
        priority = response.status match {
            case i if 200 to 299 contains i => Priority.INFO
            case i if 300 to 499 contains i => Priority.WARNING
            case _ => Priority.DANGER
          },
        info = Agent.http(response.status))
    } recover {
        case ex: Exception => {
          Logger.info(ex.getClass.getCanonicalName)
          Monitoring process event.copy(info=ex.getClass.getSimpleName)
        }
    }
  }
  
  private def monitor_cpu = {
    import java.lang.management.ManagementFactory
    import com.sun.management.OperatingSystemMXBean
    
    val osBean = ManagementFactory.getPlatformMXBean(classOf[OperatingSystemMXBean]);
    val percent = osBean.getSystemCpuLoad() * 100
      
    Monitoring process MonitoringEvent(
        target="localhost",
        source = "localhost",
        metric = "cpu.percent",
        unit = "percent",
        value = percent,
        info = "collected from OperatingSystemMXBean",
        priority = percent match { 
          case v if v < 50 => Priority.INFO
          case v if v < 80 => Priority.WARNING
          case _ => Priority.DANGER
        }
      )    
  }

  private def monitor_cpu_file = {

    import scala.io.Source

    val now = DateTime.now()

    for(line <- Source.fromFile("/proc/loadavg").getLines())
    {
      val args = line.split(" ", 4).init
      def event = MonitoringEvent(
        target="georges",
        source = "georges",
        metric = "load1",
        value = args(0).toDouble,
        info = "collected from /proc/loadavg",
        priority = if (args(0).toDouble > 1) Priority.WARNING else Priority.INFO
      )
      Monitoring process event.copy(metric = "load1", value = args(0).toDouble)
      Monitoring process event.copy(metric = "load5", value = args(1).toDouble)
      Monitoring process event.copy(metric = "load15", value = args(2).toDouble)
    }
  }


  private def monitor_mock(mock: Monitor_Mock) = {
    val value = mock.next
    Logger.info("mock: " + value)
    Monitoring process MonitoringEvent(
      target="georges",
      source = "georges",
      metric = "mock",
      value = value,
      info = "mock value",
      priority = if (value > mock.max/2) Priority.DANGER else Priority.INFO
    )

  }
}

case class Monitor_HTTP(url: String)

case class Monitor_Mock(var value: Int = 0, min: Int = 0, max: Int = 100)
{
  def next: Int = {
    value = value + 1
    if (value > max ) value = min
    value
  }


}

object Agent
{
  def props: Props = Props(classOf[Agent])

  val http = Map(
    200 -> "OK",
    404 -> "NOT FOUND"
  )
}