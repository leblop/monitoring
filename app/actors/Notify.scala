package actors

import akka.actor.{Props, Actor}
import play.api.Logger
import models.{Message, User, EmbeddedUser}
import concurrent.ExecutionContext
import play.api.i18n.Messages

import play.api.Play.current

import com.typesafe.plugin._
import events.EventBus


class Notify extends Actor
{

  implicit val global = scala.concurrent.ExecutionContext.global

  def receive = {
    case msg: Message => send(msg)
  }

  private def send(msg: Message) = {
    for {
      recipient <- User.getById(msg.recipient._id)
      sender    <- User.getById(msg.sender._id)
    } yield {
      val mail = use[com.typesafe.plugin.MailerPlugin].email
      mail.setSubject(msg.title)
      mail.setRecipient(recipient.email)
      mail.setFrom("%s<%s>".format(sender.firstname, "support@le-blop.net"))
      mail.send(msg.content)
    }
  }
}

object Notify
{
  def props: Props = Props(classOf[Notify])

  def apply(msg: Message) = EventBus.publish(msg)
}