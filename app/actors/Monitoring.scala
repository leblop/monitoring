package actors

import akka.actor.{Props, Actor}
import models.{Priority, User, Ticket, MonitoringEvent}
import events.EventBus
import api.SecuredRequest
import play.api.libs.json.JsValue
import org.joda.time.DateTime
import reactivemongo.bson.BSONObjectID


/**
 * Created by matthieu on 17/12/13.
 */
class Monitoring extends Actor
{

  private implicit val global = scala.concurrent.ExecutionContext.global

  def receive = {
    case event : MonitoringEvent => onEvent(event)
  }

  private def onEvent(event: MonitoringEvent) = {
    MonitoringEvent.insert(event)

    if(event.priority == Priority.DANGER)
    {
      val title = "%s / %s / %s".format(event.target, event.component, event.metric)
      Ticket.findBy("title", title).map {
        case Some(t) =>
        case None => createTicketFromEvent(event, title)
      }
    }
  }

  private def createTicketFromEvent(event: MonitoringEvent, title: String) =
    Ticket.next.map (
      key => Ticket.insert(ticketFromEvent(event, key, title))
    )

  private def ticketFromEvent(event: MonitoringEvent, key: String, title: String) : Ticket= {
    Ticket(
      key = key,
      title = title,
      description = event.info,
      solution = "",
      requester = User.system.toEmbedded,
      reporter =User.system.toEmbedded
    )
  }
}

object Monitoring
{
  def props: Props = Props(classOf[Monitoring])


  def process(event: MonitoringEvent) = EventBus.publish(event)
}
