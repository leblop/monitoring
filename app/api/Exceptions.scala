package api

import play.api.mvc.SimpleResult
import play.api.libs.json.{JsString, JsError, Json, JsObject, JsValue}
import reactivemongo.core.commands.LastError
import concurrent.Future
import play.api.mvc.Results._


case class HttpException(msg: String, result: SimpleResult) extends RuntimeException(msg)

object HttpException {

  def apply(code: Int, error: JsValue)    : HttpException = HttpException("error", new Status(code)(resKO(error)))

  def apply(code: Int, error: String)     : HttpException = HttpException(error, new Status(code)(resKO(error)))

  def apply(code: Int, error: JsError)    : HttpException = HttpException(Json.prettyPrint(JsError.toFlatJson(error)), new Status(code)(resKO(error)))

  def apply(code: Int, error: LastError)  : HttpException = HttpException("error", new Status(code)(resKO(error)))

  def apply(code: Int, error: Exception)  : HttpException = HttpException("error", new Status(code)(resKO(error)))


  protected def resKO(error: JsValue) : JsObject = Json.obj("error" -> error)

  protected def resKO(error: JsError) : JsObject = resKO(JsError.toFlatJson(error))

  protected def resKO(error: LastError) : JsObject = resKO(JsString("db error: %s".format(error.message)))

  protected def resKO(error: Exception) : JsObject = resKO(JsString("exception: %s".format(error.getMessage)))

  protected def resKO(error: String) : JsObject = resKO(JsString(error))
}

