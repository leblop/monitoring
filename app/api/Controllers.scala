package api

import models._
import play.api.libs.json._
import play.api.mvc._
import play.api.cache.Cache
import play.mvc.Http.Status._
import play.api.libs.json.JsSuccess


import concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import scala.Some
import play.api.mvc.SimpleResult
import helpers.converters.{TryAsFuture, JsResultAsFuture, SystemObjectToJson}
import helpers.JSONHelpers.{resKO, pagination}
import play.api.Logger
import reactivemongo.bson.BSONObjectID


/** Contains the security token, extracted from the RequestHeader */
case class SecuredRequest[A](token: String, currentUser: User, request: Request[A]) extends WrappedRequest[A](request)

trait SecuredController extends Controller {

  implicit val app: play.api.Application = play.api.Play.current
  implicit val context = scala.concurrent.ExecutionContext.global

  val AuthTokenHeader = "X-XSRF-TOKEN"
  val AuthTokenCookieKey = "XSRF-TOKEN"



  protected def hasToken(request: RequestHeader) : Option[String] = request.headers.get(AuthTokenHeader)

  protected def execute(result: => Future[SimpleResult])(implicit context: ExecutionContext) =
    result.recover {
      case e: HttpException => {
        Logger.error("Request rejected [%s] %s".format(e.result.header.status.toString, e.msg))
        e.result
      }
      case e: RuntimeException => {
        Logger.error("Request rejected - internal server error", e)
        InternalServerError(resKO(e))
      }
    }

  protected object Public extends ActionBuilder[Request] {

    def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[SimpleResult]) : Future[SimpleResult] =
      execute(block(request))
  }

  protected object Secured extends ActionBuilder[SecuredRequest] {
    import helpers.converters.TryAsFuture

    def invokeBlock[A](request: Request[A], block: (SecuredRequest[A]) => Future[SimpleResult]) : Future[SimpleResult] = {

      Logger.info("request: %s %s".format(request.method, request.path))

      execute {
        for {
          token <- tokenFromRequest(request).asFuture
          userId <- validateToken(token)
          user <- userFromCache(userId)
          secured <- isAuthorized(token, user, request)
          result <- block(secured)
        } yield result
      }
    }

    private def tokenFromRequest[A](request: Request[A]) : Try[String] = Try {
      hasToken(request).getOrElse(unauthorized("401 No Security Token"))
    }

    private def validateToken(token: String): Future[BSONObjectID] =
      Cache.getAs[String]("tokens-" + token) match {
        case Some(userId) => BSONObjectID.parse(userId).asFuture
        case None => Token.find(token) andThen {
          case Success(userId) => Cache.set("tokens-" + token, userId.stringify, 5*60)
        }
    }

    private def userFromCache(userId: BSONObjectID) : Future[User] = {
      val key = "users-" + userId.stringify
      Cache.getAs[User](key) match {
        case Some(user) => Future.successful(user)
        case None => User.getById(userId) andThen {
          case Success(_) => Cache.set(key, userId.stringify, 5*60)
        }
      }
    }

    def isAuthorized[A](token: String, user: User, request: Request[A]): Future[SecuredRequest[A]] =
    {
      // find user roles
      // find user permissions
      Logger.info("checking authorization for %s on %s: %s".format(user.username, request.method, request.path))

      retrievePermissions(user._id).flatMap {permissions =>
        (Permission.default ::: permissions).foldLeft(false)(_ || _.isAllowed(request)) match {
          case true => Future.successful(SecuredRequest(token, user, request))
          case false => Future.failed(HttpException(FORBIDDEN, request.path))
        }
      }
    }


    def retrievePermissions(userId: BSONObjectID) : Future[List[Permission]] =
      for {
        rolesID <-  AssignedRole.find(userId)
        roles   <-  Role.findById(rolesID)
      } yield roles.flatMap( _.permissions )

    def checkPermissions(permissions: List[Permission]): Try[Unit] = Try {

        forbidden("")
    }


    private def unauthorized(msg: String): Nothing = throw HttpException(UNAUTHORIZED, resKO(msg))

    private def forbidden(msg: String): Nothing = throw HttpException(FORBIDDEN, resKO(msg))
  }
/*
  protected case class Secured(val permission: String) extends Authorized
  {
    Logger.info("required permission: %s".format(permission))

    override def invokeBlock[A](request: Request[A], block: (SecuredRequest[A]) => Future[SimpleResult]) : Future[SimpleResult] = super.invokeBlock(request, block)
  }

  protected object Secured extends Authorized
  */
}



class DAOController[T <: SO](protected val dao: DAO[T], protected implicit val context: ExecutionContext) {

  import helpers.converters.JsResultAsFuture

  protected implicit val format = dao.jsonhandler
  protected implicit val handler = dao.bsonhandler

  def create(implicit request: SecuredRequest[JsValue]) : Future[SimpleResult] = createWith(Json fromJson _.body)

  def createWith(factory: SecuredRequest[JsValue] => JsResult[T])(implicit request: SecuredRequest[JsValue]) : Future[SimpleResult] =
    for {
      obj <- factory(request).asFuture
      _   <- dao.insert(obj)
    } yield Results.Created("ok")

  def list(skip: Int = 0, limit: Int = 10) =
    for {
      list <- dao.list(skip, limit)
      count <- dao.count
    } yield Results.Ok(Json.obj(
      dao.collectionName -> list,
      pagination(count, skip, limit)
    ))

  def count = dao.count.map(v => Results.Ok(Json.obj("count" -> v)))
}

class BusinessDAOController[T <: BO](override val dao: BusinessDAO[T], context: ExecutionContext) extends DAOController(dao, context) {

  implicit val ctx = context
  import helpers.JSONHelpers.validate

  override def createWith(factory: SecuredRequest[JsValue] => JsResult[T])(implicit request: SecuredRequest[JsValue]) : Future[SimpleResult] =
    for {
      obj <- factory(request).asFuture
      _   <- dao.insert(obj)
      // _   <- History.add(obj, "created")
    } yield Results.Created(Json.obj(
      "success" -> "%s created".format(obj.name),
      "url" -> obj.url
    ))

  def retrieve(name: String) = dao.getByKey(name).map( bo => Results.Ok(bo.toJson))

  def update(name: String)(implicit request: SecuredRequest[JsValue]) =
    for {
      obj   <- validate(request.body).asFuture
      _     <- dao.update(name, obj)
      // _     <- History.add(obj, "updated")
    } yield Results.Ok(Json.obj("success" -> "%s updated".format(name)))

  def delete(name: String)(implicit request: SecuredRequest[JsValue]) =
    for {
      obj     <- dao.getByKey(name)
      result  <- dao.delete(name)
      // _       <- History.add(obj, "deleted")
    } yield Results.Ok(Json.obj("success" -> "%s deleted".format(name)))

}

object DAOController {
  def get[T <: SO](dao: DAO[T])(implicit context: ExecutionContext) = new DAOController(dao, context)

  def get[T <: BO](dao: BusinessDAO[T])(implicit context: ExecutionContext) = new BusinessDAOController(dao, context)

  // def get[T <: BO](dao: DAO[T])(implicit context: ExecutionContext) = DAOController(dao, context)
}


