package models

import helpers.JSONHelpers

import reactivemongo.bson.{BSONString, BSONHandler, Macros, BSONObjectID}
import play.api.libs.json._
import reactivemongo.api.indexes.{IndexType, Index}
import org.joda.time.DateTime



object Priority extends Enumeration {
  type Priority = Value
  val DANGER, WARNING, INFO = Value

  implicit object PriorityFormat extends Format[Priority] {
    def reads(json: JsValue) = json.validate[String].map(Priority.withName)
    def writes(p: Priority) = JsString(p.toString)
  }
  implicit object PriorityHandler extends BSONHandler[BSONString, Priority] {
    def read(bson: BSONString) = Priority.withName(bson.value)
    def write(p: Priority) = BSONString(p.toString)
  }

}
import Priority._

case class MonitoringEvent(
  _id: BSONObjectID = BSONObjectID.generate,
  target: String,
  source: String,
  component: Option[String] = None,
  metric: String,
  priority: Priority = DANGER,
  value: Double,
  unit: String = "n/a",
  info: String = "none",
  start_date: DateTime = DateTime.now,
  end_date: DateTime = DateTime.now) extends SO



object MonitoringEvent extends DAO[MonitoringEvent] {

  val METRIC      = "metric"
  val SOURCE      = "source"
  val TARGET      = "target"
  val COMPONENT   = "component"
  val UNIT        = "unit"
  val VALUE       = "value"
  val INFO        = "info"
  val START_DATE  = "start_date"
  val END_DATE    = "end_date"

  def collectionName = "monitoring_events"

  implicit val context = play.api.libs.concurrent.Execution.defaultContext

  /** INDEXES */
  collection.indexesManager.ensure(Index(Seq(END_DATE -> IndexType.Descending))  )

  implicit val jsonhandler: Format[MonitoringEvent] = Json.format[MonitoringEvent]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[MonitoringEvent]

  // def fromJson(value: JsValue) = Json.fromJson[MonitoringEvent](value)

}
