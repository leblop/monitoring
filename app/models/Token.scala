package models

import reactivemongo.bson.{BSONDocument, BSONObjectID, Macros}
import play.modules.reactivemongo.ReactiveMongoPlugin
import concurrent.{Future, ExecutionContext}
import api.HttpException
import play.mvc.Http.Status._
import scala.Some
import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.core.commands.{Update, FindAndModify}
import org.joda.time.DateTime

/**
 * Created by matthieu on 03/04/14.
 */
case class Token(_id: BSONObjectID, value: String, date: DateTime)

object Token {

  import play.api.Play.current

  implicit val dateTimeHandler = helpers.converters.BSONDateTimeHandler
  implicit val bsonhandler = Macros.handler[Token]


  private val db = ReactiveMongoPlugin.db
  private lazy val tokens = db.collection[BSONCollection]("tokens")

  def find(value: String)(implicit context: ExecutionContext) : Future[BSONObjectID] =
    tokens.find(BSONDocument("value" -> value)).one[Token].flatMap(toID)

  def insert(value: String, userId: BSONObjectID)(implicit context: ExecutionContext) : Future[BSONObjectID] = {
    val selector = BSONDocument("_id" -> userId)
    val modifier = BSONDocument(
      "$set" -> BSONDocument("value" -> value),
      "$set" -> BSONDocument("date" -> DateTime.now)
    )

    val command = FindAndModify(
      "tokens",
      selector,
      Update(modifier, true),
      upsert = true)
    db.command(command).map(_.map(bsonhandler.read)).flatMap(toID)
  }

  private def toID(f: Option[Token])(implicit context: ExecutionContext) = f match {
      case Some(token) => Future.successful(token._id)
      case None         => Future.failed(HttpException(UNAUTHORIZED, "token not found"))
    }
}
