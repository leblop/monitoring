package models

import play.api.templates.{Html, HtmlFormat}
import play.api.libs.json._


case class Pagination(total: Int, skip: Int, limit: Int)
{

  def next(page: Int) = math.min(this.total, this.skip + (limit*page))

  def previous(page: Int) = math.max(0, this.skip - (limit*page))

  def first = 0

  def last = math.max(0, math.floor(total / limit)*limit).toInt

  def currentPage : Int = math.ceil(skip/limit).toInt

  def lastPage: Int = math.ceil(total/limit).toInt

  def previousPage(nb: Int): Option[Int] = (currentPage - nb) match {
    case (i: Int) if i >= 0 => Some(i)
    case _ => None
  }

  def nextPage(nb: Int) : Option[Int] = (currentPage + nb) match {
    case (i: Int) if i <= lastPage => Some(i)
    case _ => None
  }

  def range : String = {
    skip.toString + "-" + math.min(skip+limit, total).toString
  }

}

object Pagination {

  implicit val paginationFormat = Json.writes[Pagination]
}


