package models

import reactivemongo.bson.{Macros, BSONDocument, BSONObjectID}
import concurrent.{Future, ExecutionContext}
import play.modules.reactivemongo.ReactiveMongoPlugin
import reactivemongo.api.collections.default.BSONCollection
import play.api.libs.json.{Format, Json}
import org.joda.time.DateTime

/**
 * Created by matthieu on 18/03/14.
 */
case class Entry(value: String, order: Int)

object Entry {
  implicit val bsonhandler = Macros.handler[Entry]
  implicit val jsonhandler = Json.format[Entry]
}

case class Dictionary(
  _id: BSONObjectID,
  creation_date: DateTime,
  update_date: DateTime,
  key: String,
  name: String,
  default: Int,
  values: List[Entry]) extends BO
{

  def collectionName = Dictionary.collectionName

}


object Dictionary extends BusinessDAO[Dictionary]{

  import play.api.Play.current

  implicit val jsonhandler: Format[Dictionary] = Json.format[Dictionary]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[Dictionary]

  def create(name: String, default: Int, values: List[Entry]) : Dictionary = Dictionary(
    BSONObjectID.generate,
    DateTime.now,
    DateTime.now,
    name.toLowerCase.replace(" ", "_"),
    name,
    default,
    values
  )

  def collectionName = "dictionaries"
  // def key = "name"


  def values(name: String)(implicit context: ExecutionContext) : Future[List[Entry]] =
    // collection.find(BSONDocument("name" -> name)).one[Dictionary].map( _.map(_.values).getOrElse(List.empty))
    collection.find(BSONDocument("name" -> name)).one[Dictionary].map( _.map(_.values).getOrElse(List.empty))


}
