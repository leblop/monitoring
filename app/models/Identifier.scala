package models

import play.modules.reactivemongo.ReactiveMongoPlugin
import reactivemongo.api.collections.default.BSONCollection
import reactivemongo.bson.{Macros, BSONDocument}
import reactivemongo.core.commands.{Update, FindAndModify}
import concurrent.{ExecutionContext, Future}


case class Identifier(collection: String, prefix: String, current: Int) {
  def print = "%s-%d".format(prefix, current)
}

object Identifier {
  import play.api.Play.current
  import helpers.converters.OptionAsFuture

  implicit val bsonhandler = Macros.handler[Identifier]

  /** Returns the default database (as specified in `application.conf`). */
  private val db = ReactiveMongoPlugin.db
  // private lazy val identifiers = db.collection[BSONCollection]("identifiers")

  def next(collection: String, prefix: String)(implicit context: ExecutionContext) : Future[String] = {
    val selector = BSONDocument("collection" -> collection, "prefix" -> prefix)
    val modifier = BSONDocument(
      "$inc" -> BSONDocument("current" -> 1))

    val command = FindAndModify(
      "identifiers",
      selector,
      Update(modifier, true),
      upsert = true)

    db.command(command).flatMap(_.map(bsonhandler.read).map(_.print).asFuture)
  }
}

/*
import com.mongodb.casbah.Imports._


case class Identifier(
                       name: String,
                       uid: Integer) {
}

object Identifier {

  def read(dbo: DBObject): Identifier =
    Identifier(
      name = dbo.as[String]("name"),
      uid = dbo.as[Integer]("uid"))

  def write(id: Identifier): DBObject = MongoDBObject(
    "name" -> id.name,
    "uid" -> id.uid)


  def newID[T <: BusinessObject](implicit tc: MongoDbModel[T], coll: String => MongoCollection): String = {
    val ids = coll("identifiers")
    val name = tc.keyPrefix
    val fieldsMDBO = MongoDBObject.empty
    val sortMDBO = MongoDBObject("uid" -> 1)
    tc.keyPrefix + ids.findAndModify("name" $eq name, fieldsMDBO, sortMDBO, false, $inc("uid" -> 1), true, true).map(dbo => read(dbo)).get.uid
  }

}

  */