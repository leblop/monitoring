package models

import play.api.i18n.{Lang, Messages}
import play.api.templates.Html

/**
 * Created with IntelliJ IDEA.
 * User: matthieu
 * Date: 21/11/13
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
object i18n {

  def apply(key: String, args: Any*)(implicit lang: Lang) = Html(Messages(key, args : _*))
}
