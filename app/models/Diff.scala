package models

import play.api.templates.Html
import org.joda.time.DateTime
import reactivemongo.bson.{BSONDocumentWriter, BSONDocumentReader, BSONReader, BSONDocument, BSONDateTime, BSONHandler}

/**
 * Created by matthieu on 16/03/14.
 */
case class Diff[T](field: String, current: T, previous: T)
{
  def modified[B](f: (String, T, T) => B) : Option[B] = if (current != previous) Some(f(field, current, previous)) else None

  def modified: Option[T] = modified((_, current, _) => current)
}
/*
case class Modified[T](value: Option[T])

trait BSONModifiedHandler[T] extends BSONDocumentReader[Modified[T]] with BSONDocumentWriter[Modified[T]] {
  def read(doc: BSONDocument)(implicit reader: BSONReader[_ <: reactivemongo.bson.BSONValue, T]) = Modified[T](
    value =  doc.getAs[T]("mod")
  )
  def write(mod: Modified[T]) = mod.value.map(v => BSONDocument("mod" -> v)).getOrElse(BSONDocument())
}

object Modified {
  implicit object BSONModifiedStringHandler extends BSONModifiedHandler[String]
}
*/


trait Operation[T <: BO] {
  def author: User
  def update_date: DateTime
  def action: String
}

trait HistoryEntry[T <: BO] {
  def author: EmbeddedUser
  def update_date: DateTime
  def action: String
}