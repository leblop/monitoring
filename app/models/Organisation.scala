package models

import reactivemongo.bson.{Macros, BSONObjectID}
import org.joda.time.DateTime
import play.api.libs.json.{Format, Json}


case class Organisation(
  _id: BSONObjectID,
  creation_date: DateTime,
  update_date: DateTime,
  key: String,
  name: String,
  description: String
) extends BO
{
  def collectionName = Organisation.collectionName
}

object Organisation extends BusinessDAO[Organisation]
{
  implicit val jsonhandler: Format[Organisation] = Json.format[Organisation]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[Organisation]

  def collectionName = "organisations"
}

/*
object Organisation extends BusinessDAO[Organisation]{

  implicit val jsonConverter: Format[Organisation] = Json.format[Organisation]

  def collectionName = "organisations"

  val NAME = "name"
  val DESCRIPTION = "description"

  /** Organisation validator */
  private def validateOrganisation = __.json.update( (
    (__ \ NAME).json.pickBranch and
      (__ \ DESCRIPTION).json.pickBranch
    ) reduce)

  override def validateCreate =
    super.validateCreate andThen validateOrganisation

  override def validateUpdate =
    super.validateUpdate andThen validateOrganisation // andThen JSONHelpers.toMongoUpdate

}
*/
