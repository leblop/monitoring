package models
    /*
import play.modules.reactivemongo.MongoController
import concurrent.{Await, Future}
import reactivemongo.core.commands.{LastError, Count}
import reactivemongo.bson.BSONDocument
import play.api.libs.json._
import play.api.mvc.Controller
import play.api.http.Status._
import helpers.JSONHelpers
import play.api.Logger
import reactivemongo.bson.BSONObjectID
import api.JSONController
import processors.{PlainProcessor, Processor}
import helpers.JSONHelpers._
import play.modules.reactivemongo.json.collection.{JSONQueryBuilder, JSONCollection}
import reactivemongo.api.collections.GenericQueryBuilder
import play.modules.reactivemongo.json.BSONFormats

import scala.util.Failure
import scala.Some
import play.api.libs.json.JsNumber
import scala.util.Success
import processors.PlainProcessor
import play.modules.reactivemongo.json.collection.JSONCollection
import reactivemongo.api.QueryOpts
import play.api.libs.json.JsObject



trait GenericDAO[T <: SystemObject] extends Controller with MongoController with JSONController {

  protected implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

  // import play.api.libs.concurrent.Execution.Implicits._

  def collectionName: String

  lazy val collection: JSONCollection = db.collection[JSONCollection](collectionName)

  type QueryBuilder = GenericQueryBuilder[JsObject, Reads, Writes]

  def url(id: String) = "/" + collectionName + "/" + id

  def collectionSize(query: Option[JsObject] = None): Future[Int] =
    db.command(Count(collectionName, query.flatMap(b => BSONFormats.toBSON(b).asOpt.map(_.asInstanceOf[BSONDocument]))))

  protected def processList(x: List[JsObject]) : JsValue = Json.toJson(x.map( _.transform(toJSON).getOrElse(JsNull)).filter(_ != JsNull))

  protected def db_op(f: JsValue => Future[LastError])(p: PlainProcessor): Processor =
      p.toFuture {
        f(p.value).map{
          lastError => lastError.inError match {
            case true => {
              Logger.error(s"DB operation did not perform successfully: [lastError=$lastError]")
              p.http(INTERNAL_SERVER_ERROR).content(resKO(lastError))
            }
            case false => p
          }
        } recover {
          case exception: Exception => p.http(INTERNAL_SERVER_ERROR).content(resKO(exception))
        }
      }


  def db_query(builder: QueryBuilder)(p: PlainProcessor) : Processor =
  {
    val limit: Int = builder.options.batchSizeN
    p.toFuture{
      val cursor = builder.cursor[JsObject]
      val list : Future[List[JsObject]] = cursor.collect[List](limit)
      list.map(processList).map(
          objects => p |+ addFields((__ \ collectionName) -> objects)
        )
    }
  }

  protected def db_create(p: PlainProcessor) = db_op(v => collection.insert(v))(p)

  protected def db_update(id: String)(p: PlainProcessor) =
    (p
      |+ transform(JSONHelpers.toMongoUpdate)
      |+ db_op(v => collection.update(toOID(id), v))
      |+ transform(JSONHelpers.fromMongoUpdate)
    )

  protected def db_delete(id: String)(p: PlainProcessor) = db_op( _ => collection.remove(toOID(id)))(p)

  protected def db_retrieve(id: String)(p: PlainProcessor) : Processor = {
    p.toFuture(
      collection
        .find(toOID(id))
        .cursor[JsObject]
        .headOption.map {
        case None => p.http(NOT_FOUND).content(resKO(s"not found: ID $id"))
        case Some(obj) => p.content(obj)
      }
    )
  }

  protected def db_parse(id: String)(p: PlainProcessor) : Processor =
    BSONObjectID.parse(id) match {
      case Failure(e) => p.http(BAD_REQUEST).content(resKO(s"Invalid ID: $id"))
      case Success(_) => p
    }

}

trait SystemDAO[T <: SystemObject] extends GenericDAO[T] {

  val ID = "_id"

  def removeID : Reads[JsObject] = (__ \ ID).json.prune

  def generateID : Reads[JsObject] = __.json.update( generateOID(__ \ ID) )

  def toJSON : Reads[JsObject]= JSONHelpers.toJSON
  def validateCreate : Reads[JsObject] = JSONHelpers.toBSON andThen generateID
  def validateUpdate : Reads[JsObject] = JSONHelpers.toBSON andThen removeID

  implicit val jsonConverter: Format[T]

  def fromJson(value: JsValue): Option[T] = value.transform(JSONHelpers.toBSON).map{ js =>
    js.validate[T](jsonConverter).map(Some(_)).recoverTotal
    {
      error => Logger.error("Error converting to object: " + Json.prettyPrint(JsError.toFlatJson(error))); None
    }
  }.recoverTotal
  {
    error => Logger.error("Error transforming to BSON: " + Json.prettyPrint(JsError.toFlatJson(error))); None
  }

  def findById(id: String): Future[Option[T]] =
  {
    BSONObjectID.parse(id) match {
      case Failure(t: Throwable) => Future.failed(t)
      case Success(_) =>
        collection.find[JsValue](toOID(id)).cursor[T].headOption
    }
  }


  /** -------------- API ------------- **/

  private val COUNT = (__ \ 'count)
  private val PAGINATION = ( __ \ 'pagination)

  protected def addPagination(builder: QueryBuilder)(p: PlainProcessor) =
    p.toFuture{
      val skip = builder.options.skipN
      val limit = builder.options.batchSizeN
      collectionSize(builder.queryOption).map( count => p |+ addFields( PAGINATION -> Json.toJson(Pagination(count, skip, limit))))
    }

  def count(processor: PlainProcessor): Processor =
    processor.toFuture {
      collectionSize(None).map(i => processor |+ addFields(COUNT -> JsNumber(i)))
    }

  def list(skip: Int = 0, limit: Int = 10)(p: PlainProcessor): Processor = find(Json.obj(), skip, limit)(p)

  def find(query: JsObject = Json.obj(), skip: Int = 0, limit: Int = 10)(p: PlainProcessor): Processor =
  {
    Logger.info("skip: " + skip + "/ limit: " + limit)
    val builder : QueryBuilder = collection
      .find(query)
      .options(QueryOpts(skipN = skip, batchSizeN = limit))
      .sort(Json.obj(ID -> -1))
    find(builder)(p)
  }

  def find(builder: QueryBuilder)(p: PlainProcessor): Processor =
  {
    (p
      |+ addPagination(builder)
      |+ db_query(builder)
      )
  }
/*
  def look(query: JsObject = Json.obj(), skip: Int = 0, limit: Int = 10) =
  {
    Logger.info("skip: " + skip + "/ limit: " + limit)
    val builder : QueryBuilder = collection
      .find(query)
      .options(QueryOpts(skipN = skip, batchSizeN = limit))
      .sort(Json.obj(ID -> -1))


    collectionSize(builder.queryOption).map( count => Json.obj( PAGINATION -> Json.toJson(Pagination(count, skip, limit))))
    val cursor = builder.cursor[JsObject]
    val list : Future[List[JsObject]] = cursor.collect[List](limit)
    list.map(processList).map(
      objects => Json.obj(collectionName -> objects)
    )
  }

  def look(builder: QueryBuilder):  =
  {
     addPagination(builder)
     db_query(builder)
  }
  */


  def create(p: PlainProcessor) = p |+ transform(validateCreate) |+ db_create |+ http(CREATED)
  def retrieve(id: String)(processor: PlainProcessor) = processor http FORBIDDEN
  def update(id: String)(processor: PlainProcessor) =   processor http FORBIDDEN
  def delete(id: String)(processor: PlainProcessor) =   processor http FORBIDDEN

}

trait BusinessDAO[T <: BusinessObject] extends SystemDAO[T]{


  val CREATION_DATE = "creation_date"
  val UPDATE_DATE = "update_date"



  def generateCreationDate = __.json.update( generateDate(__ \ CREATION_DATE))
  def generateUpdateDate = __.json.update( generateDate(__ \ UPDATE_DATE))

  override def validateCreate = super.validateCreate andThen generateCreationDate andThen generateUpdateDate

  override def validateUpdate = super.validateUpdate andThen generateUpdateDate

  protected def history(action:String, id: String)(p: PlainProcessor) : Processor =
  {
    implicit val user = p.user
    (Processor(OK, Json.obj(HistoryEntry.ACTION -> action))
      |+ transform(HistoryEntry.init(url(id), p.value))
      |+ HistoryEntry.create
      )
    return p
  }

  override def create(processor: PlainProcessor) : Processor =
    (processor
      |+ transform(validateCreate)
      |+ db_create
      |+ http(CREATED)
      |+ transform(toJSON)
      |+ withId( (id, processor) => history("create", id)(processor))
      )

  override def retrieve(id: String)(processor: PlainProcessor): Processor =
    (processor
      |+ db_parse(id)
      |+ db_retrieve(id)
      |+ transform(toJSON)
      )

  override def update(id: String)(processor: PlainProcessor) : Processor =
    (processor
      |+ transform(validateUpdate)
      |+ db_update(id)
      |+ history("update", id)
      )

  override def delete(id: String)(processor: PlainProcessor): Processor =
    (processor
      |+ db_delete(id)
      |+ history(url(id), "delete")
      )

}
*/
