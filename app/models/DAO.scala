package models

import play.api.libs.json.{JsError, JsSuccess, JsValue, Format}
import play.modules.reactivemongo.ReactiveMongoPlugin
import concurrent.{ExecutionContext, Future}
import reactivemongo.core.commands.{Count, LastError}
import api.HttpException
import play.mvc.Http.Status._
import play.api.mvc.Results._
import scala.Some
import play.api.mvc.SimpleResult
import reactivemongo.bson.{BSONWriter, BSONValue, BSONObjectID, BSONDocumentWriter, BSONDocumentReader, BSONHandler, BSONDocument}
import util.{Failure, Success, Try}
import reactivemongo.api.QueryOpts
import reactivemongo.api.collections.default.BSONCollection
import play.api.Logger
import org.joda.time.DateTime



trait DAO[T <: SO] {

  import play.api.Play.current

  implicit protected val dateTimeConverter = helpers.converters.BSONDateTimeHandler
  implicit protected val idFormat = helpers.converters.BSONObjectIDFormat

  type BSONDocumentHandler = BSONDocumentReader[T] with BSONDocumentWriter[T] with BSONHandler[BSONDocument, T]
  type JSONDocumentHandler = Format[T]

  // used for conversion from JsValue to T and opposite
  implicit val jsonhandler : JSONDocumentHandler

  // used for conversion from BSONDocument to T and opposite
  implicit val bsonhandler : BSONDocumentHandler

  /** Returns the default database (as specified in `application.conf`). */
  def db = ReactiveMongoPlugin.db

  def collectionName: String

  protected lazy val collection = db.collection[BSONCollection](collectionName)

  protected def db_op(operation: => Future[LastError])(implicit context: ExecutionContext) : Future[Unit] =
    operation.flatMap {
      r => r.inError match {
        case true   =>  Future.failed(HttpException(INTERNAL_SERVER_ERROR, "db error: %s".format(r.message)))
        case false  =>  Future.successful()
      }
    }

  def fromJson(value: JsValue): Try[T] = value.validate[T] match {
    case JsSuccess(s, _)  => Success(s)
    case e: JsError       => Failure(HttpException(BAD_REQUEST, e))
  }

  def collectionSize(query: Option[BSONDocument] = None)(implicit context: ExecutionContext): Future[Int] =
    db.command(Count(collectionName, query))

  def insert(obj: T)(implicit context: ExecutionContext) : Future[Unit] =
    db_op(collection.insert(obj)(bsonhandler, context))

  def list(skip: Int = 0, limit: Int = 10)(implicit context: ExecutionContext) =
    collection
      .find(BSONDocument())
      .options(QueryOpts(skipN = skip, batchSizeN = limit))
      .sort(BSONDocument("_id" -> -1))
      .cursor[T]
      .collect[List](limit)

  def count(implicit context: ExecutionContext) : Future[Int] = collectionSize(None)

  def findById(id: String)(implicit context: ExecutionContext): Future[Option[T]] =
  {
    BSONObjectID.parse(id) match {
      case Failure(e) => Future.failed(e)
      case Success(bsonId) => findById(bsonId)
    }
  }

  def findById(id: BSONObjectID)(implicit context: ExecutionContext): Future[Option[T]] = findBy("_id", id)

  def findBy[F](field: String, value: F)(implicit context: ExecutionContext, writer: BSONWriter[F, _ <: reactivemongo.bson.BSONValue]): Future[Option[T]] = collection.find(BSONDocument(field -> value)).one[T]

  def getById(id: BSONObjectID)(implicit context: ExecutionContext) : Future[T] = findById(id).flatMap{
    case None => Future.failed(HttpException(NOT_FOUND, "object with id '%s' not found".format(id.stringify)))
    case Some(v) => Future.successful(v)
  }

  def getBy[F](field: String, value: F)(implicit context: ExecutionContext, writer: BSONWriter[F, _ <: reactivemongo.bson.BSONValue]) : Future[T] = findBy(field, value).flatMap{
    case None => Future.failed(HttpException(NOT_FOUND, "object with field '%s'=='%s' not found".format(field, value.toString)))
    case Some(v) => Future.successful(v)
  }

}

trait BusinessDAO[T <: BO] extends DAO[T]
{
  def key: String = "key"

  def findByKey(value: String)(implicit context: ExecutionContext) : Future[Option[T]] =
  {
    Logger.info("looking for %s=%s".format(key, value))
    collection
      .find(BSONDocument(key -> value))
      .one[T]
  }

  def getByKey(value: String)(implicit context: ExecutionContext) : Future[T] =
    findByKey(value).flatMap {
      case None       => Future.failed(HttpException(NOT_FOUND, value))
      case Some(obj)  => Future.successful(obj)
    }

  def update(name: String, obj: T)(implicit context: ExecutionContext) : Future[Unit] =
    db_op { collection.update(BSONDocument(key -> name), obj) }


  def delete(name:String)(implicit context: ExecutionContext) : Future[Unit] =
    db_op { collection.remove(BSONDocument(key -> name)) }

}


