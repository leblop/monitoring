package models


import play.api.libs.json._
import reactivemongo.bson.{BSONDocument, Macros, BSONObjectID}

import org.joda.time.DateTime
import concurrent.ExecutionContext
import actors.Notify


case class Message  (
  _id: BSONObjectID,
  creation_date: DateTime,
  recipient: EmbeddedUser,
  sender: EmbeddedUser,
  title: String,
  content: String
) extends SO
{
  // def asJSON = Json.toJson(this)(Message.jsonConverter)
}


object Message extends DAO[Message]{

  val RECIPIENT = "recipient"
  val SENDER = "sender"
  val TITLE = "title"
  val CONTENT = "content"

  def collectionName = "messages"

  implicit val jsonhandler: Format[Message] = Json.format[Message]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[Message]

  def myMessages(userId: BSONObjectID)(implicit context: ExecutionContext) =
    collection
      .find(BSONDocument("recipient._id" -> userId))
      .cursor[Message]
      .collect[List]()

  def send(recipient: EmbeddedUser, title: String, content: String)(implicit sender: EmbeddedUser, context: ExecutionContext) =
  {
    val msg = Message(
      _id = BSONObjectID.generate,
      creation_date = DateTime.now,
      recipient = recipient,
      sender = sender,
      title = title,
      content = content
    )
    insert(msg).map(_ => Notify(msg))
  }
}