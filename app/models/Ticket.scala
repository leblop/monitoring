package models

import org.joda.time.DateTime
import play.api.libs.json.{Format, Json}
import reactivemongo.bson.{BSONDocument, BSONWriter, BSONObjectID, Macros}
import api.SecuredRequest
import concurrent.{ExecutionContext, Future}


case class Ticket(
  val _id: BSONObjectID = BSONObjectID.generate,
  val creation_date: DateTime = DateTime.now,
  val update_date: DateTime = DateTime.now,
  val key: String,
  val title: String,
  val description: String,
  val solution: String,
  val requester: EmbeddedUser,
  val reporter: EmbeddedUser,
  val assignee: Option[EmbeddedUser] = None,
  val urgency: Int = Ticket.urgency.default,
  val impact: Int = Ticket.impact.default,
  val priority: Int = Ticket.priority.default,
  val status: Int = Ticket.status.default
) extends BO
{
  def collectionName = Ticket.collectionName

  def touch = this.copy(update_date = DateTime.now)

  def name = key
}

object Ticket extends BusinessDAO[Ticket]
{
  implicit val jsonhandler: JSONDocumentHandler = Json.format[Ticket]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[Ticket]

  def collectionName = "tickets"

  def next(implicit context: ExecutionContext) : Future[String] = Identifier.next(collectionName, "TICKET")

  /** should be taken from db */

  val urgency = Dictionary.create("ticket.urgency", 1, List.empty)
  val impact = Dictionary.create("ticket.impact", 1, List.empty)
  val priority = Dictionary.create("ticket.priority", 1, List.empty)
  val status = Dictionary.create("ticket.status", 0, List.empty)

}


// could be generated with macro
case class TicketDiff(
  action: String,
  update_date: DateTime,
  author: User,
  key: Diff[String],
  title: Diff[String],
  description: Diff[String],
  solution: Diff[String],
  requester: Diff[EmbeddedUser],
  reporter: Diff[EmbeddedUser],
  assignee: Diff[Option[EmbeddedUser]]) extends Operation[Ticket]

object TicketDiff {
  def apply(action: String, current: Ticket, previous: Ticket)(implicit request: SecuredRequest[_]) : TicketDiff = TicketDiff(
    action = action,
    update_date = current.update_date,
    author = request.currentUser,
    key = Diff("key", current.key, previous.key),
    title = Diff("title", current.title, previous.title),
    description = Diff("description", current.description, previous.description),
    solution = Diff("solution", current.solution, previous.solution),
    requester = Diff("requester", current.requester, previous.requester),
    reporter = Diff("reporter", current.reporter, previous.reporter),
    assignee = Diff("assignee", current.assignee, previous.assignee)
  )

  def apply(current: Ticket, previous: Ticket)(implicit request: SecuredRequest[_]) : TicketDiff = TicketDiff("update", current, previous)

  def apply(current: Ticket)(implicit request: SecuredRequest[_]) : TicketDiff = TicketDiff("create", current, current)
}

// could be generated with macro
case class TicketHistory (
                           _id: BSONObjectID,
                           entry_date: DateTime,
                           author: EmbeddedUser,
                           uid: String,
                           action: String,
                           details: Ticket
                           ) extends History[Ticket]

// could be generated with macro
object TicketHistory extends HistoryDAO[Ticket, TicketHistory]
{
  import play.modules.reactivemongo.json.BSONFormats.BSONDocumentFormat
  implicit val jsonhandler: JSONDocumentHandler = Json.format[TicketHistory]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[TicketHistory]
}
