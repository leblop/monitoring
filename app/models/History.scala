package models

/**
 * Created with IntelliJ IDEA.
 * User: matthieu
 * Date: 29/11/13
 * Time: 23:04
 * To change this template use File | Settings | File Templates.
 */

import reactivemongo.bson.{BSONDocumentWriter, BSONDocument, Macros, BSONObjectID}
import play.api.mvc.SimpleResult
import api.SecuredRequest
import concurrent.{Future, ExecutionContext}
import org.joda.time.DateTime
import play.api.libs.json.{Format, Json}
import reactivemongo.api.QueryOpts

trait History[T<:BO] extends SO
{
  def _id: BSONObjectID
  def entry_date: DateTime
  def author: EmbeddedUser
  def uid: String
  def action: String
  def details: T
}


trait HistoryDAO[T <: BO, H <: History[T]] extends DAO[H] {

  def collectionName = "history"
  def key = "_id"

  def apply(_id: BSONObjectID,
            entry_date: DateTime,
            author: EmbeddedUser,
            uid: String,
            action: String,
            details: T) : H

  def add(o: T, action: String)(implicit request: SecuredRequest[_], context: ExecutionContext, writer: BSONDocumentWriter[T]) : Future[Unit] =
    insert(apply(
      BSONObjectID.generate,
      o.update_date,
      request.currentUser.toEmbedded,
      o.toResource.uid,
      action,
      o))

  def find(uid: String)(implicit context: ExecutionContext) = collection
    .find(BSONDocument("uid" -> uid))
    // .options(QueryOpts(skipN = skip, batchSizeN = limit))
    .sort(BSONDocument("_id" -> -1))
    .cursor[H]
    .collect[List]()
}



