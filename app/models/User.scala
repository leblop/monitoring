package models

import reactivemongo.bson.{BSONDocument, Macros, BSONObjectID}

import helpers.{BCryptHelper}
import concurrent.{ExecutionContext, Future}
import org.joda.time.DateTime
import scala.Option
import play.api.libs.json.{JsValue, Format, Json}
import play.api.data.Form
import play.api.data.Forms._
import api.HttpException
import play.api.http.Status

case class EmbeddedUser (_id: BSONObjectID, url: String, name: String)
{
}

object EmbeddedUser {

  import helpers.converters.BSONObjectIDFormat
  implicit val jsonConverter = Json.format[EmbeddedUser]
  implicit val bsonhandler = Macros.handler[EmbeddedUser]
}


case class User (
  _id: BSONObjectID,
  creation_date: DateTime,
  update_date: DateTime,
  username: String,
  firstname: String,
  lastname: String,
  email: String
) extends BO
{
  def toEmbedded = EmbeddedUser(_id, url, "%s %s".format(firstname, lastname))

  def name = username

  def key = username

  def collectionName = User.collectionName
}


object User extends BusinessDAO[User] {

  def collectionName = "users"
  implicit val jsonhandler: JSONDocumentHandler = Json.format[User]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[User]

  override def key = "username"

  // import play.api.libs.concurrent.Execution.Implicits._
/*
  val anonymous = User(
    _id = BSONObjectID.generate,
    creation_date = DateTime.now,
    update_date = DateTime.now,
    username = "anonymous",
    firstname = "anonymous",
    lastname= "anonymous",
    email = "anonymous@example.com"
  )
*/
  val system = User(
    _id = BSONObjectID.generate,
    creation_date = DateTime.now,
    update_date = DateTime.now,
    username = "system",
    firstname = "system",
    lastname= "system",
    email = "system@example.com"
  )


/*
  val USERNAME = "username"
  val FIRST_NAME = "firstname"
  val LAST_NAME = "lastname"
  val EMAIL = "email"
  val PASSWORD = "password"
  val ORGANISATION_ID = "organisation_id"

  implicit val jsonConverter : Format[User] = Json.format[User]

  /** User validator */
  private def validateUser = __.json.update((
    (__ \ USERNAME).json.pickBranch(Reads.of[JsString]) and
      (__ \ FIRST_NAME).json.pickBranch(Reads.of[JsString]) and
      (__ \ LAST_NAME).json.pickBranch(Reads.of[JsString]) and
      (__ \ EMAIL).json.pickBranch(Reads.of[JsString])
    ) reduce)

  private def generatePassword =
    __.json.update( (__ \ PASSWORD).json.put( JsString("admin")))

  private def hashPassword =
    __.json.update(
      // (__ \ PASSWORD).json.put(JsString(BCryptHelper.hash(password)))
        (__ \ PASSWORD).json.pickBranch(Reads.of[JsString]).map{
          a => Json.obj(PASSWORD -> BCryptHelper.hash((a \ PASSWORD).as[String]))
        }
    )

  private def removePassword = (__ \ PASSWORD).json.prune

  override def toJSON = super.toJSON andThen removePassword

  override def validateCreate =
    super.validateCreate andThen
    validateUser andThen
    generatePassword

  val emptyObj = __.json.put(Json.obj())

  override def validateUpdate =
    super.validateUpdate andThen
      __.json.update((
        ((__ \ USERNAME).json.pickBranch(Reads.of[JsString]) or emptyObj) and
          ((__ \ FIRST_NAME).json.pickBranch(Reads.of[JsString]) or emptyObj) and
            ((__ \ LAST_NAME).json.pickBranch(Reads.of[JsString]) or emptyObj) and
              ((__ \ EMAIL).json.pickBranch(Reads.of[JsString]) or emptyObj)
        ) reduce)
   */

  // ----------------------- API ----------------------


  def authenticate(username: String, password: String)(implicit context: ExecutionContext): Future[User] =
    User.getByKey(username).flatMap(Password.verify(password))

/*
  def register(user: Register)(implicit context: ExecutionContext): Future[Unit] =
  {
    db_op{
      collection.insert(user)
    }
  } /*todo: handle errors */
*/
}

case class Password(
  _id: BSONObjectID,
  value: String) extends SO

object Password extends DAO[Password]
{
  implicit val jsonhandler: JSONDocumentHandler = Json.format[Password]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[Password]

  def collectionName = "passwords"

  def verify(provided: String)(user: User)(implicit context: ExecutionContext): Future[User] =
    for {
      hash <- getById(user._id)
      _ <- check(hash.value, provided)
    } yield user

  def check(hash: String, provided: String) =
    BCryptHelper.matches(hash, provided) match {
      case true   => Future.successful()
      case false  => Future.failed(HttpException(Status.UNAUTHORIZED, "username and/or password is invalid"))
    }
}

