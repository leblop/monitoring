package models

import reactivemongo.bson.{BSONDocument, Macros, BSONObjectID, BSONDateTime}
import play.api.libs.json._
import play.api.libs.json.JsObject
import play.api.mvc.{Results, AsyncResult, Action, Result}
import play.api.mvc.Results.{Ok, Forbidden}
import org.joda.time.DateTime
import concurrent.{ExecutionContext, Future}
import api.SecuredRequest

case class Comment  (
  _id: BSONObjectID,
  creation_date: DateTime,
  update_date: DateTime,
  author: EmbeddedUser,
  uid: String,
  content: String
) extends SO


object Comment extends DAO[Comment]{

  val AUTHOR = "author"
  val CONTENT = "content"
  val RESOURCE = "resource"

  def collectionName = "comments"

  implicit val jsonhandler: Format[Comment] = Json.format[Comment]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[Comment]

  def add[T <: BO](o: T, content: String)(implicit request: SecuredRequest[_], context: ExecutionContext) : Future[Unit] =
  {
    val now = DateTime.now
    insert(Comment(
      BSONObjectID.generate,
      now,
      now,
      request.currentUser.toEmbedded,
      o.toResource.uid,
      content))
  }

  def find(uid: String)(implicit context: ExecutionContext) =
    collection.find(BSONDocument("uid" -> uid))
      // .options(QueryOpts(skipN = skip, batchSizeN = limit))
    .sort(BSONDocument("_id" -> -1))
    .cursor[Comment]
    .collect[List]()

  // import play.api.libs.functional.syntax._

  val contentReads : Reads[String] = (__ \ "content").read[String]



  /*
  implicit val jsonConverter: Format[Comment] = Json.format[Comment]

  val removeAuthor = (__ \ AUTHOR).json.prune

  def addResource(name: String, url: String) =
    __.json.update( (
      (__ \ RESOURCE \ Resource.NAME).json.put( JsString("")) and
        (__ \ RESOURCE \ Resource.URL).json.put(JsString(url))) reduce )


  def addAuthor(implicit user: User)  = removeAuthor andThen EmbeddedUser.validateCreate((__ \ AUTHOR), user)

  private val validateComment = __.json.update( (
    (__ \ RESOURCE).json.pickBranch and
      (__ \ CONTENT).json.pickBranch
    ).reduce)

  override val validateCreate =
    super.validateCreate andThen
    Resource.validateCreate(__ \ RESOURCE) andThen
    validateComment

*/


  /** ******************** API ********************/

  // def find(url:String)(processor: PlainProcessor) : Processor = processor |+ find(Json.obj(RESOURCE + "." + Resource.URL -> url))
}
