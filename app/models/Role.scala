package models

import play.api.libs.json.{Json, Format}
import reactivemongo.bson.{BSONDocument, Macros, BSONObjectID}
import org.joda.time.DateTime
import play.modules.reactivemongo.ReactiveMongoPlugin
import reactivemongo.api.collections.default.BSONCollection
import concurrent.{Future, ExecutionContext}
import api.SecuredRequest
import play.api.mvc.Request

case class Permission(method: String, path: String, name: String)
{
  def isAllowed(request: Request[_]): Boolean = isAllowed(request.method, request.path)

  def isAllowed(method: String, path: String): Boolean = (this.method.isEmpty || this.method == method) && path.startsWith(this.path)
}


object Permission {

  implicit val jsonhandler = Json.format[Permission]
  implicit val bsonhandler = Macros.handler[Permission]

  val admin = Permission(method = "", path = "/", name = "admin")

  val monitoring = Permission(method = "GET", path = "/api/monitoring", name = "monitoring")
  val ticketing = Permission(method = "", path = "/api/tickets", name = "ticketing")
  val users = Permission(method = "", path = "/api/users", name = "users")
  val organisations = Permission(method = "", path = "/api/organisations", name = "organisations")


  val views = Permission(method = "GET", path = "/views/", name = "views")
  val ping = Permission(method = "GET", path = "/api/ping", name = "ping")
  val profile = Permission(method = "GET", path = "/api/profile", name = "profile")

  val default = List(views, ping, profile, admin, monitoring, ticketing, users, organisations)

  val assignable = List(admin, monitoring, ticketing, users, organisations)
}

case class AssignedRole(_id: BSONObjectID, roles: List[BSONObjectID])

object AssignedRole {
  import play.api.Play.current

  implicit val dateTimeHandler = helpers.converters.BSONDateTimeHandler
  implicit val bsonhandler = Macros.handler[AssignedRole]


  private val db = ReactiveMongoPlugin.db
  private lazy val roles = db.collection[BSONCollection]("user.roles")

  def find(userId: BSONObjectID)(implicit context: ExecutionContext) : Future[List[BSONObjectID]] =
    roles.find(BSONDocument("_id" -> userId)).one[AssignedRole].map( _.map(_.roles).getOrElse(List.empty))
}

case class Role(
  _id: BSONObjectID,
  creation_date: DateTime,
  update_date: DateTime,
  key: String,
  name: String,
  permissions: Set[Permission]) extends BO
{
  def collectionName = Role.collectionName
}

object Role extends BusinessDAO[Role]
{
  implicit val jsonhandler: Format[Role] = Json.format[Role]
  implicit val bsonhandler: BSONDocumentHandler = Macros.handler[Role]

  def collectionName = "roles"

  def findById(roles: List[BSONObjectID])(implicit context: ExecutionContext) : Future[List[Role]] =
    collection.find(BSONDocument("_id" -> BSONDocument("$in" -> roles))).cursor[Role].collect[List]()

}
