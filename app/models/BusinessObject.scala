package models


import scala.Predef._
import helpers.JSONHelpers
import helpers.JSONHelpers._
import play.api.libs.json._
import reactivemongo.bson.{Macros, BSONObjectID, BSONDateTime}
import org.joda.time.DateTime

// you need this import to have combinators
import play.api.libs.functional.syntax._
import play.api.libs.json.Reads._

trait SO {
  def _id: BSONObjectID
}

trait BO extends SO{

  def key: String
  def name: String
  def collectionName: String

  def url = "/%s/%s".format(collectionName, key)
  def uid = "/%s/%s".format(collectionName, _id.stringify)

  def creation_date: DateTime
  def update_date: DateTime

  def toResource = Resource(key, uid)
}


trait Link
{
  def name: String
  def uid: String
}

case class Resource (
  name: String,
  uid: String
 ) extends Link
{
  def toJson = Json.toJson(this)(Resource.resourceFormat)
}

object Resource {
  implicit val resourceFormat = Json.format[Resource]
  implicit val bsonhandler = Macros.handler[Resource]
}