package events

/**
 * Created by matthieu on 20/12/13.
 */
trait MonitoringEvent extends BaseEvent{

}


object MonitoringEvent {
  val MONITORING_EVENT = "/monitoring"

  // import processors.PlainProcessor

  // private case class Create(processor: PlainProcessor) extends events.MonitoringEvent with events.operation.Create
/*
  def fire(event: String) = {
    event match {
      case MONITORING_EVENT => EventBus publish Create(processor)
    }
  }
*/
}