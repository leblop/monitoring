package events

import reactivemongo.bson.BSONDateTime
import models.User
import akka.event.{EventStream, SubchannelClassification, ActorEventBus}
import akka.actor.ActorRef
import play.api.Logger
//import processors.{Processor, PlainProcessor}


/**
 * Created by matthieu on 17/12/13.
 */
trait BaseEvent {

  val eventTime: BSONDateTime = BSONDateTime(System.currentTimeMillis)

  // def processor: PlainProcessor
}

trait EventTrigger[T <: BaseEvent] {
  def fire(event: T): Unit = EventBus publish event
}

// case class Event(processor: PlainProcessor) extends BaseEvent

package operation {
  trait Create {
    self: BaseEvent =>
  }

  trait Retrieve {
    self: BaseEvent =>
  }

  trait Update {
    self: BaseEvent =>
  }

  trait Delete {
    self: BaseEvent =>
  }

}


/*
object EventBus extends ActorEventBus with SubchannelClassification {
  import akka.util.Subclassification

  type Event = BaseEvent
  type Classifier = String
  // type Subscriber = ActorRef

  protected def classify(event: Event): Classifier = event.channel

  protected def subclassification = new Subclassification[Classifier] {
    def isEqual(x: Classifier, y: Classifier): Boolean = {
      Logger.debug("isEqual x=" + x + " / y=" + y)
      x == y
    }

    /**
     * True if and only if x is a subclass of y; equal classes must be considered sub-classes!
     */
    def isSubclass(x: Classifier, y: Classifier): Boolean = {
      Logger.debug("isSubclass x=" + x + " / y=" + y)
      x.startsWith(y)
    }
  }

  protected def publish(event: Event, subscriber: Subscriber): Unit = {
    Logger.info("publishing event: " + event)
    subscriber ! event
  }
}
*/

object EventBus extends EventStream {

}