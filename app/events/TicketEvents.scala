package events

//import processors.{Processor, PlainProcessor}
import models.{TicketDiff, User, Ticket}
import play.api.libs.json.Json
import concurrent.Future
import play.api.http.Status.OK
import concurrent.ExecutionContext.Implicits.global


trait TicketEvent extends BaseEvent
{
  def name: String
  def ticket: TicketDiff
  implicit def user : User

  // def refreshTicket : Future[Option[Ticket]] = Ticket.findById(ticket._id)
}

case class TicketDefault(name: String, ticket: TicketDiff, user : User)  extends TicketEvent

case class TicketCreate(ticket: TicketDiff, user : User) extends TicketEvent with events.operation.Create {
  def name : String = TicketEvent.TICKET_CREATE
}
case class TicketUpdate(ticket: TicketDiff, user : User) extends TicketEvent with events.operation.Update
{
  def name : String = TicketEvent.TICKET_UPDATE
}
case class TicketDelete(ticket: TicketDiff, user : User) extends TicketEvent with events.operation.Delete
{
  def name : String = TicketEvent.TICKET_DELETE
}

object TicketEvent extends EventTrigger[TicketEvent]
{
  val TICKET_CREATE = "/tickets/create"
  val TICKET_RETRIEVE = "/tickets/retrieve"
  val TICKET_UPDATE = "/tickets/update"
  val TICKET_DELETE = "/tickets/delete"
}