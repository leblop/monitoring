package helpers

import play.api.libs.json._
import org.joda.time.DateTime
import reactivemongo.bson.{BSONDocument, BSONString, BSONNull, BSONWriter, BSONReader, BSONValue, BSONDateTime, BSONHandler, BSONObjectID}
import play.api.data.validation.ValidationError
import scala.util.{Failure, Success, Try}
import concurrent.Future
import api.HttpException
import play.mvc.Http.Status._
import models.{EmbeddedUser, Pagination, SO}
import reactivemongo.core.commands.LastError
import play.api.mvc.Results
import play.modules.reactivemongo.json.BSONFormats.{BSONStringFormat, PartialFormat}
import play.api.libs.json.Json.JsValueWrapper
import reactivemongo.bson.DefaultBSONHandlers.BSONStringHandler

/**
 * Created with IntelliJ IDEA.
 * User: matthieu
 * Date: 27/11/13
 * Time: 13:13
 * To change this template use File | Settings | File Templates.
 */

object converters
{
  implicit class TryAsFuture[T](val attempt: Try[T]) extends AnyVal {
    def asFuture: Future[T] = attempt match {
      case Success(v) => Future successful v
      case Failure(f) => Future failed f
    }
  }

  implicit class OptionAsFuture[T](val attempt: Option[T]) extends AnyVal {
    def asFuture: Future[T] = attempt match {
      case Some(v) => Future successful v
      case None => Future failed HttpException(NOT_FOUND, "option not found")
    }
  }

  implicit class JsResultAsFuture[T](val attempt: JsResult[T]) extends AnyVal {
    def asFuture: Future[T] = attempt match {
      case JsSuccess(v, _) => Future successful v
      case f: JsError => Future failed HttpException(BAD_REQUEST, f)
    }
  }

  implicit class SystemObjectToJson[T <: SO](val obj: T) extends AnyVal {
    def toJson(implicit wrt: Writes[T]): JsValue = Json.toJson(obj)
  }

  // implicit conversion joda DateTime <-> BSON DateTime
  implicit object BSONDateTimeHandler extends BSONHandler[BSONDateTime, DateTime] {
    def read(time: BSONDateTime) = new DateTime(time.value)
    def write(jdtime: DateTime) = BSONDateTime(jdtime.getMillis)
  }

  // implicit conversion
  implicit object BSONObjectIDFormat extends PartialFormat[BSONObjectID] {
    def partialReads: PartialFunction[JsValue, JsResult[BSONObjectID]] = {
      case JsString(v) => JsSuccess(BSONObjectID(v))
    }
    val partialWrites: PartialFunction[BSONValue, JsValue] = {
      case oid: BSONObjectID => JsString(oid.stringify)
    }
  }
}

object JSONHelpers {

  // protected def resOK(key: String)(value: Int) = Results.(Json.obj(key -> JsNumber(value)))

  def resKO(error: JsValue) : JsObject = Json.obj("error" -> error)

  def resKO(error: JsError) : JsObject = resKO(JsError.toFlatJson(error))

  def resKO(error: LastError) : JsObject = resKO(JsString("exception %s".format(error.getMessage)))

  def resKO(error: Exception) : JsObject = resKO(JsString("exception %s".format(error.getMessage)))

  def resKO(error: String) : JsObject = resKO(JsString(error))



  def success(msg: String) : (String, JsValueWrapper) = "success" -> msg

  def url(url: String) : (String, JsValueWrapper) = "url" -> url

  def pagination(count: Int, skip: Int, limit: Int) : (String, JsValueWrapper) = "pagination" -> Json.toJson(Pagination(count, skip, limit))


  def validate[T](value: JsValue)(implicit rds: Reads[T]) : Try[T] =
    value.validate[T] match {
      case JsSuccess(s, _)  => Success(s)
      case e: JsError       => Failure(HttpException(BAD_REQUEST, e))
    }

  // def resKO(code: Int, error: JsValue) : Future[SimpleResult] = Future.successful(new Status(code)(Json.obj("error" -> error)))

  // protected def resKO(code: Int, error: JsError) : Future[SimpleResult] = resKO(code, JsError.toFlatJson(error))

/*
  def oidToJSON(path: JsPath) = __.json.update((path).json.copyFrom( (path \ '$oid).json.pick ))

  def generateDate(path: JsPath) = (path \ '$date).json.put( JsNumber(DateTime.now.getMillis ))

  def generateOID(path: JsPath) = (path \ '$oid).json.put( JsString(BSONObjectID.generate.stringify))

  val toMongoUpdate = (__ \ '$set).json.copyFrom( __.json.pick )

  val fromMongoUpdate = __.json.copyFrom( (__ \ '$set).json.pick)

  trait jsonConverter extends Reads[JsObject]
  {
    def reads(js: JsValue): JsResult[JsObject] = {
      js match {
        case obj: JsObject =>  parseObject(obj, __)
        case _ => JsError (__, ValidationError("validation.invalid", "not a JsObject"))
      }
    }

    protected type Field = (String, JsValue)

    private def parseObject(obj: JsObject, parent: JsPath): JsResult[JsObject] = {
      parseFields(obj.fields.toList, parent).map(list => JsObject(list.toSeq))
    }

    private def parseFields(l: List[Field], parent: JsPath): JsResult[List[Field]] = {
      l match {
        case Nil => JsSuccess(Nil, parent)
        case head :: tail => merge(
          parseField(head, parent).map(head._1 -> _),
          parseFields(tail, parent)
        )
      }
    }

    private def parseField(field: Field, parent: JsPath): JsResult[JsValue] =
    {
      applyConversion(field, parent).getOrElse
      {
        field._2 match {
          case v: JsObject => parseObject(v, parent \ field._1)
          case v: JsArray => JsSuccess(null)
          case v: JsString => JsSuccess(v, parent \ field._1)
          case v: JsNumber => JsSuccess(v, parent \ field._1)
          case v: JsBoolean => JsSuccess(v, parent \ field._1)
          case JsNull => JsSuccess(null)
          case _: JsUndefined => JsSuccess(null)
        }
      }
    }

    protected def applyConversion(field: Field, parent: JsPath) : Option[JsResult[JsValue]]
    /**
    private def parse(arr: JsArray, parent: JsPath): JsResult[JsValue] = {
      parse(arr.value.toList, parent, 0).map(Json.toJson(_))
    }

    private def parse(l: List[JsValue], parent: JsPath, i: Int): JsResult[List[JsValue]] = {
      l match {
        case Nil => JsSuccess(Nil)
        case head :: tail => merge(parse(head, parent(i)), parse(tail, parent, i + 1))
      }
    }
      **/

    private def merge(head: JsResult[Field], tail: JsResult[List[Field]]): JsResult[List[Field]] = {
      (head, tail) match {
        case (h: JsError, t: JsError) => h ++ t
        case (JsSuccess(h, p1), JsSuccess(t, p2)) =>
        {
          JsSuccess(h :: t)
        }
        case (h: JsError, _) => h
        case _ => tail
      }
    }
  }

  val toJSON = new jsonConverter {
    def applyConversion(field: Field, parent: JsPath) : Option[JsResult[JsValue]] =
    {
      field match {
        case (key: String, v: JsObject) if (key.endsWith("_date")) => Some(JsSuccess(v.fields(0)._2, parent))
        case (key: String, v :JsObject) if (key.endsWith("_id")) =>  Some(JsSuccess(v.fields(0)._2, parent))
        case _ => None
      }
    }
  }

  val toBSON = new jsonConverter {

    def applyConversion(field: Field, parent: JsPath) : Option[JsResult[JsValue]] = {
      field match {
        case (key: String, v: JsNumber) if (key.endsWith("_date")) => Some(JsSuccess(Json.obj("$date" -> v), parent))
        case (key: String, v :JsString) if(key.endsWith("_id")) => Some(JsSuccess(Json.obj("$oid" -> v), parent))
        case _ => None
      }
    }
  }
  */
}
