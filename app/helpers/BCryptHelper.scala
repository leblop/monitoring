package helpers

import org.mindrot.jbcrypt.BCrypt

/**
 * Created with IntelliJ IDEA.
 * User: matthieu
 * Date: 27/11/13
 * Time: 10:20
 * To change this template use File | Settings | File Templates.
 */
object BCryptHelper {
    val log_rounds: Int = 10

    def hash(plainPassword: String): String = {
      // val logRounds = app.configuration.getInt(RoundsProperty).getOrElse(DefaultRounds)
      BCrypt.hashpw(plainPassword, BCrypt.gensalt(log_rounds))
    }

    def matches(hashedPassword: String, inputPassword: String):Boolean = {
      BCrypt.checkpw(inputPassword, hashedPassword)
    }
}
