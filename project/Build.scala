import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName         = "monitoring"
  val appVersion      = "1.0-SNAPSHOT"

  // scalaVersion := "2.11.6"
  scalaVersion := "2.10.5"

  val appDependencies = Seq(
    // Add your project dependencies here,
    // jdbc,
    // anorm,
    cache
    ,"org.reactivemongo" %% "play2-reactivemongo" % "0.10.5.0.akka22"
    ,"com.typesafe" %% "play-plugins-mailer" % "2.2.0"
    ,"org.mindrot" % "jbcrypt" % "0.3m"
        // exclude("org.scala-stm", "scala-stm_2.10.0")
        // exclude("play", "*"),
    // ,"securesocial" %% "securesocial" % "2.1.1"
      // exclude("org.scala-stm", "scala-stm_2.10.0")
      // exclude("play", "*")
    )

//  val main = Project(appName, file(".")).enablePlugins(play.PlayScala).settings(
//    version := appVersion,
//    libraryDependencies ++= appDependencies
//    resolvers += Resolver.url("sbt-plugin-releases", new URL("http://repo.scala-sbt.org/scalasbt/sbt-plugin-releases/"))(Resolver.ivyStylePatterns)
//  )  

  val main = play.Project(appName, appVersion, appDependencies).settings(
    // Add your own project settings here
    resolvers += Resolver.url("sbt-plugin-releases", new URL("http://repo.scala-sbt.org/scalasbt/sbt-plugin-releases/"))(Resolver.ivyStylePatterns)
  )



}
