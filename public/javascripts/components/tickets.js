// ---------------- TICKETS ---------------------- //



angular.module('tickets', ['ngResource', 'ngRoute', 'services.pagination'])
.factory('TicketModel', ['$resource',
  function($resource){
    return $resource('/api/tickets/:id', {id:'@id'}, {
        query: {method:'GET', isArray:false},
        update: {method:'PUT'},
        addComment: {method: 'POST'}
        })
  }])
.factory('CommentTicketModel', ['$resource',
    function($resource){
      return $resource('/api/tickets/:id/comments', {id:'@id'}, {})
    }])
.controller('TicketsController', ['$scope', '$routeParams', 'TicketModel', 'CommentTicketModel', 'pagination', '$http',
  function ($scope, $routeParams, TicketModel, CommentTicketModel, pagination, $http) {
    $scope.tickets = [];
    $scope.ticket = new TicketModel();
    // $scope.ticket_org = {}
    $scope.pagination = {};
    $scope.comments = []
    $scope.comment = new CommentTicketModel();
    $scope.editmode = false;
    $scope.history = []
    // $scope.priorities = {}
    $scope.urgency = {}


    $scope.create = function() {
        TicketModel.save($scope.ticket, function(){
            $scope.loadTickets()
            $scope.ticket = new TicketModel();
        })
    }

    $scope.retrieve = function() {
        TicketModel.get({id: $routeParams.id}, function(data){
            // $scope.ticket_org = angular.copy(data)
            $scope.ticket = angular.copy(data);
            $scope.editmode=false;
            $scope.ticketForm.$setPristine();
            $http.get('/api/dictionaries/ticket.urgency').success( function(response) {
                $scope.urgency = response
            })
            $http.get('/api/dictionaries/ticket.priority').success( function(response) {
                $scope.priorities = response
                $scope.extractedPriority = _.find($scope.priorities.values, function(entry) {return entry.order == $scope.ticket.priority}).value
            })
            $http.get('/api/dictionaries/ticket.status').success( function(response) {
                $scope.statuses = response
                $scope.extractedStatus = _.find($scope.statuses.values, function(entry) {return entry.order == $scope.ticket.status}).value
            })
        }, function(data, status) {
            $scope.alerts.danger("unable to retrieve ticket: " + data.error)
        })
            // CommentTicketModel.query({id: $routeParams.id}, function(data){
        $http.get('/api/tickets/' + $routeParams.id + '/comments').success( function(response) {
            $scope.comments = response.comments
        })

        $http.get('/api/tickets/' + $routeParams.id + '/history').success( function(response) {
            $scope.history = response.history
        })
    }

    $scope.update = function() {
        // diff($scope.ticket, $scope.ticket_org)
        TicketModel.update({id: $routeParams.id}, $scope.ticket, function(data) {
            $scope.retrieve()
        })
    }

    $scope.delete = function() {
        TicketModel.delete({id: $routeParams.id})
    }

    $scope.addComment = function() {
        CommentTicketModel.save({id: $routeParams.id}, $scope.comment, $scope.retrieve)
    }

    $scope.loadTickets = function() {

        TicketModel.query({skip: $routeParams.skip , limit: $routeParams.limit}, function(data){
            $scope.tickets = data.tickets;
            $scope.pagination = pagination.create(data.pagination);
        })
      };

}])
.config(["$routeProvider", function($routeProvider) {
          return $routeProvider
            .when("/tickets", {
            templateUrl: "/views/tickets",
            controller: 'TicketsController'
         }).when("/tickets/:id", {
            templateUrl: "/views/ticket",
            controller: "TicketsController"
         })
 }])