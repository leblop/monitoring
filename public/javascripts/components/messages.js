
// ---------------- MESSAGES ---------------------- //

angular.module('messages', ['ngResource', 'ngRoute', 'services.pagination'])
.controller('MessagesController', ['$scope', '$routeParams', '$http', 'pagination',
    function ($scope, $routeParams, $http, pagination) {
        $scope.messages = [];
        $scope.message = {};
        $scope.pagination = {};

        $scope.loadMessages = function() {
            $http.get('/api/messages', {params: {skip: $routeParams.skip , limit: $routeParams.limit}})
            .success(function(data, status, headers, config) {
              $scope.messages = data.messages;
              $scope.pagination = pagination.create(data.pagination);
            })
        };
    }])
.config(["$routeProvider", function($routeProvider) {
    return $routeProvider
        .when("/messages", {
            templateUrl: "/views/messages",
            controller: 'MessagesController'
    })
}])

