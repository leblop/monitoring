// ---------------- USERS ---------------------- //

angular.module('users', ['ngResource', 'ngRoute', 'services.pagination'])
.factory('UserModel', ['$resource',
  function($resource){
    return $resource('/api/users/:id', {id:'@id'}, {
        query: {method:'GET', isArray:false},
        update: {method:'PUT'}
        })
  }])
.controller('UsersController', ['$scope', '$routeParams', 'UserModel', 'pagination', '$http',
  function ($scope, $routeParams, UserModel, pagination, $http) {
    $scope.users = [];
    $scope.organisations = [];

    $scope.user = new UserModel();
    $scope.user_org = {}
    $scope.pagination = {};
    $scope.editmode = false;
    $scope.history = []


    $scope.create = function() {
        UserModel.save($scope.user, function(){
            $scope.loadUsers()
            $scope.user = new UserModel();
        })
    }

    $scope.retrieve = function() {
        UserModel.get({id: $routeParams.id}, function(data){
            // $scope.user_org = angular.copy(data)
            $scope.user = angular.copy(data);
            $scope.editmode=false;
            $scope.userForm.$setPristine();
        }, function(data, status) {
            $scope.alerts.danger("unable to retrieve user: " + data.error)
        })
        $http.get('/api/users/' + $routeParams.id + '/history').success( function(response) {
            $scope.history = response.history
        })
        $http.get('/api/organisations').success(function(data, status) {
            $scope.organisations = data.organisations
        })

    }

    $scope.update = function() {
        // diff($scope.user, $scope.user_org)
        UserModel.update({id: $routeParams.id}, $scope.user, function(data) {
            $scope.retrieve()
        })
    }

    $scope.delete = function() {
        $scope.user.$delete({id: $routeParams.id})
    }

    $scope.loadUsers = function() {
        UserModel.query({skip: $routeParams.skip , limit: $routeParams.limit}, function(data){
            $scope.users = data.users;
            $scope.pagination = pagination.create(data.pagination);
        })
      };
}])
.config(["$routeProvider", function($routeProvider) {
          return $routeProvider.
          when("/users", {
              templateUrl: "/views/users",
              controller: 'UsersController'
          }).when("/users/:id", {
              templateUrl: "/views/user",
              controller: "UsersController"
          })
}])
