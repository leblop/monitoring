// ---------------- ORGANISATIONS ---------------------- //

angular.module('organisations', ['ngResource', 'ngRoute', 'services.pagination'])
.factory('OrganisationModel', ['$resource',
      function($resource){
        return $resource('/api/organisations/:id', {id:'@id'}, {
            query: {method:'GET', isArray:false},
            update: {method:'PUT'}
            })
      }])
.config(["$routeProvider", function($routeProvider) {
  return $routeProvider
      .when("/organisations", {
          templateUrl: "/views/organisations",
          controller: 'OrganisationsController'
      }).when("/organisations/:id", {
          templateUrl: "/views/organisation",
          controller: "OrganisationsController"
      })
}])
.controller('OrganisationsController', ['$scope', '$routeParams', 'OrganisationModel', 'pagination',
    function ($scope, $routeParams, OrganisationModel, pagination) {
        $scope.organisations = [];
        $scope.organisation = new OrganisationModel();
        $scope.pagination = {};

        $scope.create = function() {
            $scope.organisation.$save(function(){
                $scope.loadOrganisations()
                $scope.organisation = new OrganisationModel();
            })
        }

        $scope.retrieve = function() {
            OrganisationModel.get({id: $routeParams.id}, function(data)
            {
                $scope.organisation = data
            })
        }

        $scope.update = function() {
            $scope.organisation.$update({id: $routeParams.id}, function() {
                $scope.retrieve()
                $scope.alerts.success("Organisation updated.")
            })
        }

        $scope.delete = function() {
            $scope.organisation.$delete({id: $routeParams.id})
        }

        $scope.loadOrganisations = function() {
            OrganisationModel.query({skip: $routeParams.skip , limit: $routeParams.limit}, function(data)
            {
                $scope.organisations = data.organisations;
                $scope.pagination = pagination.create(data.pagination);
            })
        };

        $scope.loadOrganisations();
    }])


