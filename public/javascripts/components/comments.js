
// ---------------- COMMENTS ---------------------- //

angular.module('comments', ['ngResource', 'ngRoute', 'services.pagination'])
.factory('CommentModel', ['$resource',
  function($resource){
    return $resource('/api/comments/:id', {id:'@id'}, {
        query: {method:'GET', isArray:false}
        })
    }])
.controller('CommentsController', ['$scope', '$routeParams', 'CommentModel', 'pagination',
    function ($scope, $routeParams, CommentModel, pagination) {
        $scope.comments = [];
        $scope.comment = new CommentModel();
        $scope.pagination = {};

        $scope.loadComments = function() {
            CommentModel.query({skip: $routeParams.skip , limit: $routeParams.limit}, function(data){
              $scope.comments = data.comments;
              $scope.pagination = pagination.create(data.pagination);
            })
        };
    }])
.config(["$routeProvider", function($routeProvider) {
    return $routeProvider
        .when("/comments", {
            templateUrl: "/views/comments",
            controller: 'CommentsController'
    })
}])

