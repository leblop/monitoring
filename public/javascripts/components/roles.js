// ---------------- ORGANISATIONS ---------------------- //

angular.module('roles', ['ngResource', 'ngRoute', 'services.pagination'])
.factory('RoleModel', ['$resource',
      function($resource){
        return $resource('/api/roles/:id', {id:'@id'}, {
            query: {method:'GET', isArray:false},
            update: {method:'PUT'}
            })
      }])
.config(["$routeProvider", function($routeProvider) {
  return $routeProvider
      .when("/roles", {
          templateUrl: "/views/roles",
          controller: 'RolesController'
      }).when("/roles/:id", {
          templateUrl: "/views/role",
          controller: "RolesController"
      })
}])
.controller('RolesController', ['$scope', '$routeParams', 'RoleModel', 'pagination', '$http',
    function ($scope, $routeParams, RoleModel, pagination, $http) {
        $scope.roles = [];
        $scope.role = new RoleModel();
        $scope.pagination = {};
        $scope.permissions = [];
        $scope.permission = {};
        $scope.editmode = false;

        $scope.create = function() {
            $scope.role.$save(function(){
                $scope.loadRoles()
                $scope.role = new RoleModel();
            })
        }

        $scope.retrieve = function() {
            RoleModel.get({id: $routeParams.id}, function(data)
            {
                $scope.editmode = false;
                $scope.roleForm.$setPristine();
                $scope.role = data
                $http.get('/api/roles/!permissions').success( function(response) {
                    $scope.permissions = _.difference(response.permissions, $scope.role.permissions)

                })
            })
        }

        $scope.update = function() {
            $scope.role.$update({id: $routeParams.id}, function() {
                $scope.retrieve()
            })
        }

        $scope.delete = function() {
            $scope.role.$delete({id: $routeParams.id})
        }

        $scope.loadRoles = function() {
            RoleModel.query({skip: $routeParams.skip , limit: $routeParams.limit}, function(data)
            {
                $scope.roles = data.roles;
                $scope.pagination = pagination.create(data.pagination);
            })
        };

        $scope.addPermission = function() {
            $scope.role.permissions.push($scope.permission)
            $scope.roleForm.$setDirty();
        }

        $scope.removePermission = function(index) {
            $scope.role.permissions.splice(index, 1);
            $scope.roleForm.$setDirty();
        }

        $scope.loadRoles();
    }])


