
// ---------------- EVENTS ---------------------- //

angular.module('events', ['ngRoute', 'services.pagination'])
    .controller('EventController', ['$scope', '$routeParams', '$http', 'pagination',
        function ($scope, $routeParams, $http, pagination) {
            $scope.events = [];
            $scope.pagination = {};

            $scope.loadEvents = function() {
                $http.get('/api/events', {params: {skip: $routeParams.skip , limit: $routeParams.limit}})
                .success(
                    function(data){
                        $scope.events = data.monitoring_events;
                        $scope.pagination = pagination.create(data.pagination);
                    })
            };

            $scope.alertLevel = function(priority) {
                if (priority == 'DANGER') return "alert alert-danger"
                if (priority == 'WARNING') return "alert alert-warning"
                return "alert alert-success"
            }
    }])
    .config(['$routeProvider', function($routeProvider) {
        return $routeProvider
            .when("/events", {
                templateUrl: "/views/events",
                controller: 'EventController'
            })
    }])

