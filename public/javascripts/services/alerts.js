angular.module('services.alerts', []).factory('alerts', [function(){

  var alertsService = {};
  alertsService.alerts = []

    alertsService.last = function() {
        return alertsService.alerts[alertsService.alerts.length - 1]
    }

    alertsService.add = function(type, message) {
        now = new Date().getTime()
        last = alertsService.last()
        if (last != null && message == last.message && (last.time > (now - 3000)  ))
        {
            last.count += 1
            last.time = now
        }
        else
            alertsService.alerts.push({time: now, type: type, message: message, count: 1});
    }

    alertsService.success = function(message) {
        alertsService.add("alert-success", message)
    }
    alertsService.info = function(message) {
        alertsService.add("alert-info", message)
    }
    alertsService.warning = function(message) {
        alertsService.add("alert-warning", message)
    }
    alertsService.danger = function(message) {
        alertsService.add("alert-danger", message)
    }
    alertsService.clear = function(index) {
        alertsService.alerts.splice(index, 1)
    }
    alertsService.all = function() {
        return alertsService.alerts;
    }
  return alertsService;
}]);


