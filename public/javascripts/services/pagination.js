// ------------- generic pagination mechanism used by all controllers ------------------


function CheckPagination(location, info) {
    this.location = location;
    this.total = info.total;
    this.skip = info.skip;
    this.limit = info.limit;
    this.next = function(page){
        return Math.min(this.total, this.skip + (this.limit*page));
    };
    this.previous = function(page) {
        return Math.max(0, this.skip - (this.limit*page));
    };
    this.first = 0;
    this.last = Math.max(0, Math.floor(this.total / this.limit)*this.limit);
    this.currentPage = Math.ceil(this.skip/this.limit);
    this.lastPage = Math.ceil(this.total/this.limit);
    this.nextPage = function(page) { return (this.currentPage + page); };

    this.loadFirstPage = function(){
        this.location.search({skip: this.first, limit: this.limit})
    };
    this.loadNextPage = function(page){
        this.location.search({skip: this.next(page), limit: this.limit})
    };
    this.loadPreviousPage = function(page){
        this.location.search({skip: this.previous(page), limit: this.limit})
    };
    this.loadLastPage = function(page){
        this.location.search({skip: this.last, limit: this.limit})
    };
    this.range = this.skip + "-" + Math.min(this.skip+this.limit, this.total)
};



angular.module('services.pagination', []).service('pagination', ['$location', function($location){
    this.create = function(info) {
        return new CheckPagination($location, info)
    }
}]);