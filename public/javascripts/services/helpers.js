
var diff = function(mod, org) {
    _.each(_.pairs(org), function(element, index, list) {
        if ((_.isString(element[1]) || _.isNumber(element[1])) && _.isEqual(mod[element[0]], element[1]))
            delete mod[element[0]]
        else if (_.isObject(element[1]))
        {
            diff(mod[element[0]], element[1])
            if(_.isEmpty(mod[element[0]]))
                delete mod[element[0]]
        }

    })
}