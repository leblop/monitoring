angular.module('services.login',['ngRoute', 'ngCookies'])
  .controller('LoginController', ['$scope', '$cookies', '$http', '$q', '$timeout', '$location', '$route', '$rootScope',
    function ($scope, $cookies, $http, $q, $timeout, $location, $route, $rootScope) {

        $scope.credentials = {}
        $scope.user = {}

        $scope.register = {}
        $scope.redirectTo = $rootScope.redirectTo



        // Check token cookie and try to authenticate
        // Otherwise the user has to log in
        fetchUser = function() {
          return $http.get("/api/profile")
        }


        var token = $cookies["XSRF-TOKEN"];
        if (token) {
            $http.get("/api/ping")
            .then(
              function(response) {
                // Token valid, fetch user data
                return fetchUser()
              },
              function(response) {
                token = undefined;
                $cookies["XSRF-TOKEN"] = undefined;
                return $q.reject("Token invalid");
              }
            ).then(
              function(response) {
                $scope.user = response.data;
              }, function(response) {
                // Token invalid or fetching the user failed
              }
            );
        }

        /**
        * Login using the given credentials as (email,password).
        * The server adds the XSRF-TOKEN cookie which is then picked up by Play.
        */
        $scope.login = function(credentials) {
            console.log("login using credentials: " + credentials.username)
            $http.post("/api/login", credentials)
            .then(
                function(response) { // success
                    $scope.credentials = {}
                    token = response.data.authToken;
                    return fetchUser()
                }, function(response) { // error
                    $scope.error = response.error;
                    // return 'empty' promise so the right `then` function is called
                    return $q.reject("Login failed");
                }
            )
            .then(
                function(response) {
                    $scope.user = response.data;
                    $route.reload();
                    $scope.alerts.info("User logged in")
                },
                function(response) {
                    console.log(response);
                }
            );
        };

        /**
        * Invalidate the token on the server.
        */
        $scope.logout = function() {
            $http.post("/api/logout").then(function() {
                $scope.user = undefined;
                $location.path("/login")
                $rootScope.redirectTo = "/"
            });
        };

        /** Subscribe to the logout event emitted by the $http interceptor. */
        $scope.$on("InvalidToken", function() {
            console.log("InvalidToken!");
            $scope.user = undefined;
            $rootScope.redirectTo = $location.path()
            $location.path("/login")
       });

        $scope.signup = function() {
            $http.post("/api/register", $scope.register).then(function() {
             $scope.alerts.success("user registered")
            })
        }

  }])
  .config(["$routeProvider", function($routeProvider) {
          return $routeProvider
              .when("/register", {
                templateUrl: "/views/register",
                 controller: 'LoginController'
             })
             .when("/login", {
                templateUrl: "/views/login",
                controller: 'LoginController'
             })
          }
        ]);
