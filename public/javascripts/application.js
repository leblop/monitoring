

var app = angular.module('monitoring', [
    'ngRoute',
    'services.breadcrumbs',
    'services.alerts',
    'services.login',
    'events',
    'comments',
    'messages',
    'organisations',
    'roles',
    'users',
    'tickets'
    ])
.controller('MainController', ['$scope', 'breadcrumbs', 'alerts', '$rootScope',
    function ($scope, breadcrumbs, alerts, $rootScope) {
        $rootScope.alerts = alerts
        $rootScope.redirectTo = "/"
        $scope.alerts = $rootScope.alerts
        $scope.breadcrumbs = breadcrumbs;
       }])
    .config(["$routeProvider", function($routeProvider) {
        return $routeProvider
            .when("/", {templateUrl: "/views/index" })
            .when("/index", {templateUrl: "/views/index"})
            .when("/about", {templateUrl: "/views/about"})
            .when("/contact", {templateUrl: "/views/contact"})
            .when("/home", {templateUrl: "/views/home"})
            .when("/profile", {templateUrl: "/views/profile"})
            .when("/preferences", {templateUrl: "/views/preferences"})
            .otherwise( // {redirectTo: '/'}
            )
        }
      ])
    .config(["$locationProvider", function($locationProvider) {
          return $locationProvider.html5Mode(true).hashPrefix("!");
        }
      ])
    .config(["$httpProvider", function($httpProvider) {
        var interceptor = ["$rootScope", "$q", "$timeout", function($rootScope, $q, $timeout) {
            return function(promise) {
              return promise.then(
                function(response) {
                    if (response.data.info)
                        $rootScope.alerts.info(response.data.info)
                    if (response.data.success)
                        $rootScope.alerts.success(response.data.success)
                  return response;
                },
                function(response) {
                  if (response.status == 401) {
                    $rootScope.$broadcast("InvalidToken");
                    $rootScope.sessionExpired = true;
                    $timeout(function() {$rootScope.sessionExpired = false;}, 5000);
                  } else if (response.status == 403) {
                    $rootScope.alerts.warning("Action forbidden")
                    $rootScope.$broadcast("InsufficientPrivileges");
				  } else if (response.status == 404) {
                    $rootScope.alerts.warning("Not found")
                  } else {
                    $rootScope.alerts.danger("[" + response.status + "]" + response.data.error)
                    // Here you could handle other status codes, e.g. retry a 404
                  }
                  return $q.reject(response);
                }
              );
            };
          }];
          $httpProvider.responseInterceptors.push(interceptor);
        }])
    .directive('autofill', ['$timeout', function ($timeout) {
        return {
            scope: true,
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                           $timeout(function(){
                              $(elem[0]).trigger('input');
                              // elem.trigger('input'); try this if above don't work
                           },500)
                  }
        }
    }]);;



